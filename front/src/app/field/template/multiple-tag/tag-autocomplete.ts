import {Autocomplete} from "../multiple-parent/autocomplete";
import {TagService} from "../../../../generated/api";
import {Observable} from "rxjs";
import {map} from "rxjs/operators";
import { of } from 'rxjs/internal/observable/of';

export class TagValue{
  public constructor(
    public strValue:string
  ) {
  }
}
export class TagAutocomplete implements Autocomplete<TagValue>{
  allTags : TagValue[]

  constructor(
    public fieldUuid: string,
    public tagService: TagService,
  ){

  }


  getAllTags() : Observable<TagValue[]>{
    if(this.allTags)
      return of(this.allTags)
    else
      return this.tagService.getAllTags(this.fieldUuid).pipe(
        map(v => {
          this.allTags = v.map(i => new TagValue(i))
          return this.allTags
        })
      )
  }


  add(v: string): Observable<TagValue> {
    if(confirm("Are you sure you want to add tag : " + v)){
      return this.tagService.addTag(this.fieldUuid, v).pipe(
        map(_ => {
          this.allTags.push(new TagValue(v))
          return new TagValue(v)
        })
      )
    } else return of(undefined)
  }

  getSuggestions(v: string): Observable<TagValue[]> {
    if(!v) return this.getAllTags().pipe(map(v => v.slice()));
    const lowerCaseV = v.toLowerCase()
    return this.getAllTags().pipe(
      map( all => all.filter(i => i.strValue.toLowerCase().includes(lowerCaseV)))
    )
  }

  canAdd(): boolean {
    return true;
  }

  stringRepr(t: TagValue): string {
    return t?.strValue;
  }

  get(v: string): Observable<TagValue> {
    return this.getAllTags().pipe(
      map(l => l.find(u => u.strValue === v))
    )
  }
}

