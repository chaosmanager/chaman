import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {HttpEvent, HttpEventType} from "@angular/common/http";
import {Annex, GalleryService} from "../../../generated/api";
import {ToastService} from "../../toast/toast.service";
import {ImageGalleryComponent} from "../image-gallery/image-gallery.component";

@Component({
  selector: 'app-image-gallery-page',
  templateUrl: './image-gallery-page.component.html',
  styleUrls: ['./image-gallery-page.component.css']
})
export class ImageGalleryPageComponent implements OnInit {

  progress = undefined;
  constructor(
    public galleryService:GalleryService,
    public toastService:ToastService,
  ) { }

  @ViewChild(ImageGalleryComponent) imageGallery: ImageGalleryComponent;

  ngOnInit(): void {
  }


  onFileChange(event: any) {
    this.uploadFile(event.target.files[0]);
  }

  private uploadFile(file: any) {
    this.galleryService.uploadImage(file, 'events', true)
      .subscribe((event: HttpEvent<any>) => {
        switch (event.type) {
          case HttpEventType.ResponseHeader:
            if(event.status < 300 && event.status >= 200) {
              this.toastService.showSuccess("Image added")
            } else {
              this.toastService.showError("Error while uploading image", null)
            }
            this.progress = 0;
            break;
          case HttpEventType.UploadProgress:
            var eventTotal = event.total ? event.total : 0;
            this.progress = Math.round(event.loaded / eventTotal * 100);
            break;
          case HttpEventType.Response:
            this.imageGallery.refresh()
            setTimeout(() => {
              this.progress = 0;
            }, 1500);
        }
      })
  }

}
