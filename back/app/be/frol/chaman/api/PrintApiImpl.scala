package be.frol.chaman.api

import akka.actor.ActorSystem
import akka.stream.Materializer
import be.frol.chaman.model.PrintJobStatus
import be.frol.chaman.openapi.api.PrintApi
import be.frol.chaman.openapi.model.Event.EventType
import be.frol.chaman.openapi.model.{Event, PrintJob, Printer}
import be.frol.chaman.service.{PrintService, WebsocketService}
import be.frol.chaman.tables.Tables._
import be.frol.chaman.utils.DateUtils._
import be.frol.chaman.utils.OptionUtils.enrichedObject
import play.api.Logging
import play.api.db.slick.DatabaseConfigProvider
import play.api.mvc.{AbstractController, AnyContent, ControllerComponents, Request}

import javax.inject.Inject
import scala.concurrent.{ExecutionContext, Future}

class PrintApiImpl @Inject()(
                              val cc: ControllerComponents,
                              val dbConfigProvider: DatabaseConfigProvider,
                              val printService: PrintService,
                              val websocketService: WebsocketService,
                            )(implicit executionContext: ExecutionContext, system: ActorSystem, mat: Materializer)
  extends AbstractController(cc) with PrintApi with ParentController with DbContext with Logging {

  override def setStatus(uuid: String, status: String)(implicit request: Request[AnyContent]) = run { implicit u =>
    db.run(printService.setStatus(uuid, status)).map(toDto(_))
  }


  def toDto(v: (PrintJobRow, PrintStatusRow)): PrintJob = toDto(v._1, v._2)

  def toDto(p: PrintJobRow, s: PrintStatusRow): PrintJob = {
    PrintJob(p.uuid.toOpt, p.itemUuid.toOpt, p.printerUuid.toOpt(), s.status.toOpt)
  }

  def toDto(p: PrinterRow, s: Option[PrinterLastSeenRow] = None): Printer = {
    Printer(p.uuid.toOpt, p.identifier.toOpt(), s.map(_.timestamp.toOffsetDateTime))
  }

  override def deleteJob(uuid: String)(implicit request: Request[AnyContent]): Future[PrintJob] = run { implicit u =>
    db.run(printService.setStatus(uuid, PrintJobStatus.Cancelled.toString)).map(toDto(_))
  }

  override def addPrint(uuid: String, itemUuid: String)(implicit request: Request[AnyContent]): Future[PrintJob] = run { implicit u =>
    db.run(printService.create(uuid, itemUuid))
      .flatMap(v => websocketService.addEvent(new Event(EventType.Print.toOpt(), uuid.toOpt(), None, None), uuid).map(_ => v))
      .map(toDto(_))
  }

  override def addPrinter()(implicit request: Request[AnyContent]): Future[Printer] = run { implicit u =>
    db.run(printService.createPrinter())
      .map(toDto(_))
  }

  override def getPrinters()(implicit request: Request[AnyContent]): Future[List[Printer]] = run { implicit u =>
    db.run(
      printService.getPrinters().map(_.map(toDto(_)).toList)
    )
  }

  override def getPrints(uuid: String)(implicit request: Request[AnyContent]): Future[List[PrintJob]] = run { implicit u =>
    db.run(printService.getToPrint(uuid)).map(_.map(toDto(_)).toList)
  }

  override def deletePrinter(uuid: String)(implicit request: Request[AnyContent]): Future[Unit] = run { implicit u =>
    db.run(
      printService.deletePrinter(uuid).map(_ => None)
    )

  }

  override def updatePrinter(uuid: String, newIdentifier: String)(implicit request: Request[AnyContent]) = run {implicit u =>
    db.run(printService.updatePrinter(uuid, newIdentifier))
      .map(toDto(_))
  }
}
