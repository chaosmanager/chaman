package be.frol.chaman.openapi.api

import org.openapitools.OpenApiExceptions
import javax.inject.{Inject, Singleton}
import play.api.libs.json._
import play.api.mvc._
import scala.concurrent.{ExecutionContext, Future}
import be.frol.chaman.openapi.model.ServiceFieldValue

@javax.annotation.Generated(value = Array("org.openapitools.codegen.languages.ScalaPlayFrameworkServerCodegen"))
@Singleton
class ServiceFieldApiController @Inject()(cc: ControllerComponents, api: ServiceFieldApi)(implicit executionContext: ExecutionContext) extends AbstractController(cc) {
  /**
    * GET /api/field/:uuid/serviceFieldValues
    */
  def getAllValues(uuid: String): Action[AnyContent] = Action.async { request =>
    def executeApi(): Future[List[ServiceFieldValue]] = {
      api.getAllValues(uuid)(request)
    }

    executeApi().map { result =>
      val json = Json.toJson(result)
      Ok(json)
    }
  }

  private def splitCollectionParam(paramValues: String, collectionFormat: String): List[String] = {
    val splitBy =
      collectionFormat match {
        case "csv" => ",+"
        case "tsv" => "\t+"
        case "ssv" => " +"
        case "pipes" => "|+"
      }

    paramValues.split(splitBy).toList
  }
}
