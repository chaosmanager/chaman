import { Component, OnInit } from '@angular/core';
import {FieldParent} from "../../core/fieldParent";

@Component({
  selector: 'app-rich-text',
  templateUrl: './rich-text.component.html',
  styleUrls: ['./rich-text.component.css']
})
export class RichTextComponent extends FieldParent implements OnInit {

  constructor() {
    super()
  }

  ngOnInit(): void {
  }

}
