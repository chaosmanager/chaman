package be.frol.chaman.openapi.api

import org.openapitools.OpenApiExceptions
import javax.inject.{Inject, Singleton}
import play.api.libs.json._
import play.api.mvc._
import scala.concurrent.{ExecutionContext, Future}
import be.frol.chaman.openapi.model.ClientToken

@javax.annotation.Generated(value = Array("org.openapitools.codegen.languages.ScalaPlayFrameworkServerCodegen"))
@Singleton
class ClientTokenApiController @Inject()(cc: ControllerComponents, api: ClientTokenApi)(implicit executionContext: ExecutionContext) extends AbstractController(cc) {
  /**
    * PUT /api/client-token/
    */
  def createClientTokens(): Action[AnyContent] = Action.async { request =>
    def executeApi(): Future[ClientToken] = {
      val clientToken = request.body.asJson.map(_.as[ClientToken])
      api.createClientTokens(clientToken)(request)
    }

    executeApi().map { result =>
      val json = Json.toJson(result)
      Ok(json)
    }
  }

  /**
    * GET /api/client-token/
    */
  def getClientTokens(): Action[AnyContent] = Action.async { request =>
    def executeApi(): Future[List[ClientToken]] = {
      api.getClientTokens()(request)
    }

    executeApi().map { result =>
      val json = Json.toJson(result)
      Ok(json)
    }
  }

  /**
    * DELETE /api/client-token/:uuid
    */
  def removeClientTokens(uuid: String): Action[AnyContent] = Action.async { request =>
    def executeApi(): Future[Unit] = {
      api.removeClientTokens(uuid)(request)
    }

    executeApi().map { _ =>
      Ok
    }
  }

  private def splitCollectionParam(paramValues: String, collectionFormat: String): List[String] = {
    val splitBy =
      collectionFormat match {
        case "csv" => ",+"
        case "tsv" => "\t+"
        case "ssv" => " +"
        case "pipes" => "|+"
      }

    paramValues.split(splitBy).toList
  }
}
