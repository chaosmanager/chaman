package be.frol.chaman.openapi.model

import play.api.libs.json._

/**
  * Represents the Swagger definition for FieldConfig.
  * @param defaultField true if it is a default field that should be on every item card
  */
@javax.annotation.Generated(value = Array("org.openapitools.codegen.languages.ScalaPlayFrameworkServerCodegen"))
case class FieldConfig(
  label: Option[String],
  reference: Option[String],
  datatype: Option[String],
  config: Option[List[Field]],
  defaultField: Option[Boolean]
)

object FieldConfig {
  implicit lazy val fieldConfigJsonFormat: Format[FieldConfig] = Json.format[FieldConfig]
}

