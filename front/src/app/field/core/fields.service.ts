import { Injectable } from '@angular/core';
import {FieldParent} from './fieldParent';
import {StringComponent} from "../template/string/string.component";
import {NumberComponent} from "../template/number/number.component";
import {SelectComponent} from "../template/select/select.component";
import {MultipleStringComponent} from "../template/multiple-string/multiple-string.component";
import {MultipleNumberComponent} from "../template/multiple-number/multiple-number.component";
import {MultipleTagComponent} from "../template/multiple-tag/multiple-tag.component";
import {RichTextComponent} from "../template/rich-text/rich-text.component";
import {ServiceFieldComponent} from "../template/service-field/service-field.component";

@Injectable({
  providedIn: 'root'
})
export class FieldsService {

  fields = new Map();


  constructor() {
    this.fields.set("string", StringComponent)
    this.fields.set("number", NumberComponent)
    this.fields.set("select", SelectComponent)
    this.fields.set("multipleString", MultipleStringComponent)
    this.fields.set("multipleNumber", MultipleNumberComponent)
    this.fields.set("multipleTag", MultipleTagComponent)
    this.fields.set("richText", RichTextComponent)
    this.fields.set("serviceField", ServiceFieldComponent)
  }

  getFieldComponent(fieldType: string){
    return this.fields.get(fieldType);
  }
}
