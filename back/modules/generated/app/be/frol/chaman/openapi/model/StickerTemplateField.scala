package be.frol.chaman.openapi.model

import play.api.libs.json._

/**
  * Represents the Swagger definition for StickerTemplateField.
  */
@javax.annotation.Generated(value = Array("org.openapitools.codegen.languages.ScalaPlayFrameworkServerCodegen"))
case class StickerTemplateField(
  id: Option[String],
  weight: Option[Int]
)

object StickerTemplateField {
  implicit lazy val stickerTemplateFieldJsonFormat: Format[StickerTemplateField] = Json.format[StickerTemplateField]
}

