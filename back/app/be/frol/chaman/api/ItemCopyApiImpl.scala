package be.frol.chaman.api

import be.frol
import be.frol.chaman
import be.frol.chaman.model.UserInfo
import be.frol.chaman.openapi.api.CopyItemApi
import be.frol.chaman.openapi.model.Item
import be.frol.chaman.service._
import be.frol.chaman.tables
import be.frol.chaman.tables.Tables
import be.frol.chaman.utils.DateUtils
import be.frol.chaman.utils.OptionUtils.enrichedOption
import play.api.db.slick.DatabaseConfigProvider
import play.api.mvc.{AnyContent, ControllerComponents, Request}
import slick.dbio.DBIOAction

import java.util.UUID
import javax.inject.Inject
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future


class ItemCopyApiImpl @Inject()(
                                 cc: ControllerComponents,
                                 val dbConfigProvider: DatabaseConfigProvider,
                                 itemService: ItemService,
                                 fieldDataService: DataService,
                                 annexService: AnnexService,
                                 fieldService: FieldService,
                                 itemGetterService: ItemGetterService,
                                 thumbnailService: ThumbnailService,
                               ) extends CopyItemApi with DbContext with ParentController {

  import api._



  override def copyItem(uuid: String)(implicit request: Request[AnyContent]): Future[Item] = run { implicit u =>
    val newUuid = UUID.randomUUID().toString

    db.run((for {
      i <- itemService.get(uuid)
      annexes <- annexService.forItem(uuid)
      data <- fieldDataService.dataFor(uuid :: (annexes.map(_.uuid)).toList).result
      thumbnail <- thumbnailService.find(i.uuid)
      fields <- fieldService.getDbFields(data.map(_.fieldUuid).toSet)
      newItem <- itemService.add(i.copy(uuid = newUuid, author = u.uuid, timestamp = DateUtils.ts))
      newAnnexesUuidMap <- newAnnexes(newUuid, annexes)
      newData <- newData(newUuid, data, (List(uuid -> newUuid) ++ newAnnexesUuidMap).toMap)
      recoverNewItem <- itemGetterService.getSavedItem(newUuid)
      updThumbnail <- updateThumbnail(newUuid,thumbnail, newAnnexesUuidMap, annexes)
    } yield recoverNewItem).transactionally
    )
  }


  private def newAnnexes(newUuid: String, annexes: Seq[Tables.AnnexRow])(implicit u: UserInfo) = {
    val replacement = annexes.map(a => a.uuid -> a.copy(uuid = UUID.randomUUID().toString, itemUuid = newUuid, author = u.uuid, timestamp = DateUtils.ts))
    annexService.addAll(replacement.map(_._2)).map(_ => replacement.map(v => v._1 -> v._2.uuid))
  }

  private def newData(newUuid: String, data: Seq[tables.Tables.DataRow], uuidMap: Map[String, String])(implicit u:UserInfo) = {
    fieldDataService.addAll(data.map(a => a.copy(ownerUuid = uuidMap(a.ownerUuid), valueUuid = UUID.randomUUID().toString, author = u.uuid, timestamp = DateUtils.ts)))
  }

  def updateThumbnail(newItemUuid:String, thumbnail: Option[Tables.ItemThumbnailRow], newAnnexesUuidMap: Seq[(String, String)], annexes: Seq[Tables.AnnexRow]) (implicit u: UserInfo) = {
    thumbnail.map(t => thumbnailService.setThumbnailFromInfos(
      newItemUuid,
      annexes.find(_.uuid == t.annexUuid).getOrThrowM("Annex not found when copying thumbnail").filesha,
      newAnnexesUuidMap.toMap.apply(t.annexUuid),
      thumbnail.map(_.x),
      thumbnail.map(_.y),
      thumbnail.map(_.width),
    )).getOrElse(DBIOAction.successful(None))
  }

}
