package be.frol.chaman.service

import be.frol.chaman.Conf
import be.frol.chaman.api.DbContext
import be.frol.chaman.model.UserInfo
import be.frol.chaman.openapi.model.Thumbnail
import be.frol.chaman.tables.Tables
import be.frol.chaman.utils.DateUtils
import be.frol.chaman.utils.TryUtils.tryEnriched
import org.imgscalr.Scalr
import play.api.db.slick.DatabaseConfigProvider
import slick.util.Logging

import javax.imageio.ImageIO
import javax.inject.Inject
import scala.util.Try

class ThumbnailService @Inject()(
                                  val dbConfigProvider: DatabaseConfigProvider,
                                  val fieldService: FieldService,
                                ) extends DbContext with Logging {


  import api._

  def add(p: Tables.ItemThumbnailRow) = {
    ((Tables.ItemThumbnail returning Tables.ItemThumbnail.map(_.id)
      into ((v, id) => v.copy(id = id))) += p)
  }


  def setThumbnail(uuid: String, icon: Option[Thumbnail], annex: Tables.AnnexRow)(implicit user: UserInfo): DBIO[Tables.ItemThumbnailRow] = {
    val filesha = annex.filesha
    val annexUuid = annex.uuid
    setThumbnailFromInfos(uuid, filesha, annexUuid,icon.flatMap(_.x).map(_.toInt), icon.flatMap(_.y).map(_.toInt), icon.flatMap(_.width).map(_.toInt))
  }

  def setThumbnailFromInfos(uuid: String, filesha: String, annexUuid: String, ix: Option[Int], iy: Option[Int] , iwidth: Option[Int])(implicit user: UserInfo) : DBIO[Tables.ItemThumbnailRow]= {
    val image = Try {
      ImageIO.read(Conf.annex(filesha))
    }.getOrThrowM("Couldn't read file associated with annex")
    val width = Math.min(Math.min(iwidth.getOrElse(image.getWidth), image.getHeight), image.getWidth)
    val x = Math.min(ix.getOrElse(Int.MaxValue), (image.getWidth - width))
    val y = Math.min(iy.getOrElse(Int.MaxValue), (image.getHeight - width))
    val croppedImage = image.getSubimage(x, y, width, width)
    val scaledImage = Scalr.resize(croppedImage, Scalr.Method.BALANCED, Conf.thumbnailWidth, Conf.thumbnailWidth)
    ImageIO.write(scaledImage, "png", Conf.thumbnail(uuid));
    add(Tables.ItemThumbnailRow(0L, uuid, annexUuid, x, y, width, user.uuid, DateUtils.ts))
  }

  def hasThumbnail(uuid: String) = java.nio.file.Files.exists(Conf.thumbnail(uuid).toPath)

  def find(itemUuid: String) = lastVersion.filter(_.itemUuid === itemUuid).result.headOption

  val lastVersion = {
    Tables.ItemThumbnail.filterNot(f => Tables.ItemThumbnail.filter(_.itemUuid === f.itemUuid)
      .filter(_.id > f.id).exists)
  }
}
