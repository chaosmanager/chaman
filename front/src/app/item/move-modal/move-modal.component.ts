import {Component, Input, OnInit} from '@angular/core';
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {ItemDescr, Link, LinkService} from "../../../generated/api";
import {RxjsHelperService, RxjsHelperWrap} from "../../rxjs-helper.service";
import {combineLatest} from "rxjs";

@Component({
  selector: 'app-move-modal',
  templateUrl: './move-modal.component.html',
  styleUrls: ['./move-modal.component.css']
})
export class MoveModalComponent implements OnInit {
  origins: Link[];

  constructor(
    public modal: NgbActiveModal,
    public linkService: LinkService,
    public r: RxjsHelperService,
  ) {
  }

  ngOnInit(): void {
  }


  @Input()
  public itemToMove: ItemDescr;

  @Input()
  public itemDestination: ItemDescr;

  @Input()
  public moveMultipleItems: boolean;

  selectItemToMove(i: ItemDescr) {
    this.itemToMove = i
    this.getOrigins();
  }

  selectDest(item: ItemDescr) {
    this.itemDestination = item;
    this.getOrigins();
  }

  private getOrigins() {
    this.linkService.getLinks(this.itemToMove.uuid).toPromise().then(ls =>
      this.origins = ls.filter(l => l.holder)
    )
  }

  newPosition() {
    this.performMove(
      this.r.wrap(this.linkService.addLink(this.itemDestination.uuid, this.itemToMove.uuid, true))
    )
  }

  move(link: Link) {
    this.performMove(this.r.wrap(
      combineLatest([
        this.linkService.addLink(this.itemDestination.uuid, this.itemToMove.uuid, true),
        this.linkService.deleteLink(link.uuid)
      ])
    ));
  }

  private performMove(rxjsHelperWrap: RxjsHelperWrap<any>) {
    rxjsHelperWrap.withErrorMessage("Couldn't move item")
      .withSuccessMessage("item moved")
      .then(v => {
        if(this.moveMultipleItems){
          this.origins = undefined;
          this.itemToMove = undefined;
        } else {
          this.modal.close
          window.location.reload()
        }
      })
  }

}
