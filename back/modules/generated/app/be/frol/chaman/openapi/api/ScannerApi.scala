package be.frol.chaman.openapi.api

import play.api.libs.json._
import play.api.mvc._
import scala.concurrent.Future
import be.frol.chaman.openapi.model.Scan

@javax.annotation.Generated(value = Array("org.openapitools.codegen.languages.ScalaPlayFrameworkServerCodegen"))
trait ScannerApi {
  /**
    * add a scan
    * @param origin websocket client id origin
    */
  def addScan(uuid: String, origin: Option[String])(implicit request:Request[AnyContent]): Future[Unit]


  /**
    * scan history
    */
  def scanHistory()(implicit request:Request[AnyContent]): Future[List[Scan]]

}
