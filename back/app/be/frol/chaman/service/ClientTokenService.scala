package be.frol.chaman.service

import be.frol.chaman.api.DbContext
import be.frol.chaman.model.UserInfo
import be.frol.chaman.tables.Tables
import be.frol.chaman.tables.Tables.{ClientAccessTokenRemovedRow, UserAccessTokenRemovedRow}
import be.frol.chaman.utils.DateUtils
import be.frol.chaman.utils.OptionUtils._
import play.api.db.slick.DatabaseConfigProvider

import java.util.UUID
import javax.inject.Inject
import scala.concurrent.ExecutionContext

class ClientTokenService @Inject()(
                                  val dbConfigProvider: DatabaseConfigProvider,
                                ) extends DbContext {


  import api._



  def add(p: Tables.ClientAccessTokenRow) = {
    ((Tables.ClientAccessToken returning Tables.ClientAccessToken.map(_.id)
      into ((v, id) => v.copy(id = id))) += p)
  }


  val activeTokens = Tables.ClientAccessToken.filterNot(u => Tables.ClientAccessTokenRemoved.filter(_.fkClientAccessTokenId === u.id).exists)

  def getTokensFor(userUuid: String) = {
    activeTokens.filter(_.author === userUuid).result
  }

  def remove(uuid: String)(implicit user: UserInfo, executionContext: ExecutionContext) = {
    activeTokens.filter(_.uuid === uuid).result.headOption.map(_.getOrThrowM("No active token with id : " + uuid))
      .flatMap(v => Tables.ClientAccessTokenRemoved += ClientAccessTokenRemovedRow(v.id, user.uuid, DateUtils.ts))
  }
}


