import {Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {Field, FieldValue} from "../../../../generated/api";
import {FieldValueImpl} from "../../../model/FieldValueImpl";
import {MatChipInputEvent} from "@angular/material/chips";
import {FieldValueComponent} from "../field-value/field-value.component";

@Component({
  selector: 'app-field',
  templateUrl: './field.component.html',
  styleUrls: ['./field.component.css']
})
export class FieldComponent implements OnInit {

  constructor() {
  }

  @Input() inEdit : boolean = false
  @Input() field: Field
  @Input() showLabel: Boolean = true
  @ViewChild('fieldElement') fieldElement: FieldValueComponent;

  beforeSaveHook() : Promise<void> {
    return this.fieldElement.beforeSaveHook();
  }
  ngOnInit(): void {
  }


}
