import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Field} from "../../../generated/api";
import {DataSource} from "@angular/cdk/collections";
import {MatTableDataSource} from "@angular/material/table";

@Component({
  selector: 'field-table',
  templateUrl: './field-table.component.html',
  styleUrls: ['./field-table.component.css']
})
export class FieldTableComponent implements OnInit {

  constructor() { }

  datasource = new MatTableDataSource<Field>()
  displayedColumns = ["ref", "label", "fieldType"]
  @Input() allowEdit: boolean = false
  @Input() allowRemove: boolean = false

  @Output() editField = new EventEmitter<Field>();
  @Output() selectField = new EventEmitter<Field>();
  @Output() removeField = new EventEmitter<Field>();

  @Input() set fields(f:Array<Field>){
    this.datasource.data = f
  }

  ngOnInit(): void {
    if(this.allowEdit) {
      this.displayedColumns.push("actions")
    }
  }

  actionEditField(field:Field) : void {
    this.editField.emit(field);
  }
  actionSelectField(field:Field) : void {
    this.selectField.emit(field);
  }
  actionRemoveField(field:Field) : void {
    this.removeField.emit(field);
  }

}
