package be.frol.chaman.utils

import be.frol.chaman.utils.OptionUtils.enrichedOption
import slick.basic.BasicStreamingAction
import slick.dbio.{DBIOAction, Effect, NoStream}

import scala.concurrent.ExecutionContext

object SlickUtils {

  implicit def enrichedResultOption[R, T, E <: Effect](o: BasicStreamingAction[R, T, E]) = new AnyRef {
    def headOrThrow(e: => String)(implicit executionContext: ExecutionContext): DBIOAction[T, NoStream, E] =
      o.headOption.map(_.getOrThrowM(e))
  }

}
