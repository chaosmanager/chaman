package be.frol.chaman.api

import be.frol.chaman.Conf
import be.frol.chaman.service.{ItemService, StickerService, StickerTemplateService}
import be.frol.chaman.utils.OptionUtils.enrichedObject
import be.frol.chaman.utils.{PdfUtils, QrUtils}
import play.api.Configuration
import play.api.db.slick.DatabaseConfigProvider
import play.api.mvc._
import slick.dbio.DBIO

import java.util.Base64
import javax.inject.Inject
import scala.concurrent.ExecutionContext

class Media @Inject()(
                       c: ControllerComponents,
                       val itemService: ItemService,
                       val stickerService: StickerService,
                       val stickerTemplateService: StickerTemplateService,
                     )(
                       implicit val ec: ExecutionContext,
                       val conf: Configuration,
                       val dbConfigProvider: DatabaseConfigProvider,
                     ) extends AbstractController(c) with DbContext {

  def itemQr(id: String) = Action { r =>
    Ok(qrCode(id)).as("image/png");
  }

  private def qrCode(id: String) = {
    val stream = QrUtils.qrCodeOS(Conf.qrCodePrefix + id)
    val r = stream.toByteArray
    stream.close()
    r
  }

  def sticker(id: String) = Action.async { r =>
    db.run(
      stickerHtmlString(id, r.getQueryString("width").map(_.toFloat), r.getQueryString("height").map(_.toFloat), r.getQueryString("templateId"))
        .map(v => Ok(PdfUtils.toPdfBytes(v)).as("application/pdf"))
    )
  }

  def stickerImg(id: String) = Action.async { r =>
    val ratio = 29F / 62
    val width = r.getQueryString("width").map(_.toFloat).getOrElse(693F)
    db.run(
      stickerHtmlString(id,
        Some(width),
        r.getQueryString("height").map(_.toFloat).orElse(Some(width * ratio)), r.getQueryString("templateId"))
        .map(v => Ok(PdfUtils.toImageBytes(v, width.toInt)).as("image/png"))
    )
  }

  def stickerHtml(id: String) = Action.async { r =>
    val ratio = 29F / 62
    val width = 150F
    db.run(
      stickerHtmlString(id,
        r.getQueryString("width").map(_.toFloat).orElse(Some(width)),
        r.getQueryString("height").orElse(Some("70")).map(_.toFloat).orElse(Some(width * ratio)), r.getQueryString("templateId"))
        .map(v => Ok(v).as("text/html"))
    )
  }

  private def stickerHtmlString(id: String, width: Option[Float], height: Option[Float], templateId: Option[String]): DBIO[String] = {
    for {
      i <- itemService.get(id)
      templId <- templateId.map(id => DBIO.successful(id.toOpt())).getOrElse(stickerTemplateService.defaultStickerTemplateUuid)
      additionalInfos <- if (templId.isDefined) stickerService.infosFor(templId.get, id) else DBIO.successful(Nil)
    } yield views.html.Sticker.apply(
      Base64.getEncoder().encodeToString(qrCode(id)),
      i.uuid.take(8),
      i.title,
      i.description,
      width.getOrElse(100),
      height.getOrElse(50),
      additionalInfos
    ).toString
  }

}
