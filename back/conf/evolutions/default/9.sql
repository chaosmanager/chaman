-- remove subreference

-- !Ups

update data set owner_uuid = subreference_uuid where subreference_uuid is not null;
alter table data drop column subreference_uuid;
