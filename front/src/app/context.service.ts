import {Injectable} from '@angular/core';
import {Subject} from "rxjs";
import {ItemDescr, Printer, PrintService, User, UserService} from "../generated/api";

@Injectable({
  providedIn: 'root'
})
export class ContextService {
  public websocketClientId = Math.ceil(Math.random() * 1000000000) + ''
  public itemContext = new Subject<ItemDescr>();
  public user: User
  public printers: Array<Printer>

  constructor(
    private userService: UserService,
    private printService: PrintService,
  ) {
    this.printService.getPrinters().toPromise().then(v => this.printers = v)
  }

  initUser() {
    if (!this.user) {
      this.userService.getUser()
        .toPromise().then(u => this.user = u)
    }
  }

}

