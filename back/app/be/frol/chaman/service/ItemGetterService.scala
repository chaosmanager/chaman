package be.frol.chaman.service

import be.frol.chaman.api.DbContext
import be.frol.chaman.mapper.ItemMapper
import be.frol.chaman.model.RichModelConversions.fieldToFieldWithConf
import be.frol.chaman.model.UserInfo
import be.frol.chaman.tables.Tables
import be.frol.chaman.utils.DateUtils
import be.frol.chaman.utils.OptionUtils.enrichedOption
import play.api.db.slick.DatabaseConfigProvider
import play.api.mvc.ControllerComponents

import javax.inject.Inject
import scala.concurrent.ExecutionContext

class ItemGetterService @Inject()(
                                   val dbConfigProvider: DatabaseConfigProvider,
                                   itemService: ItemService,
                                   fieldDataService: DataService,
                                   annexService: AnnexService,
                                   linkService: LinkService,
                                   fieldService: FieldService,
                                 )(
                                   implicit executionContext: ExecutionContext,
                                   val dataTypeService: FieldTypeService,
                                 ) extends DbContext {


  import api._

  def getSavedItem(uuid: String) = {
    for {
      i <- itemService.getOpt(uuid)
      annexes <- annexService.forItem(uuid)
      links <- linkService.getLinksAndLinkedItem(uuid)
      data <- fieldDataService.dataFor(uuid :: (annexes.map(_.uuid) ++ links.map(_._1.uuid)).toList).result
      fields <- fieldService.getDbFields(data.map(_.fieldUuid).toSet)
      defaultFields <- fieldService.getDefaultFields()
    } yield {
      val fieldsIdSet = fields.map(_.uuid).toSet
      ItemMapper.toDtoFDOptR(uuid,i, (defaultFields.filterNot(f => fieldsIdSet.contains(f.uuid)) ++ fields).map(fieldToFieldWithConf), data, annexes, links)
    }

  }
}
