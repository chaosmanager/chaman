import {Component, OnInit} from '@angular/core';
import {MatTableDataSource} from "@angular/material/table";
import {ClientToken, ClientTokenService, UserToken} from "../../../../generated/api";

class ClientTokenImpl implements ClientToken {
  constructor(
    id?: string,
    date?: string,
    value?: string,
    accessPoint?: string,
  ) {
  }
}

@Component({
  selector: 'app-client-tokens-page',
  templateUrl: './client-tokens-page.component.html',
  styleUrls: ['./client-tokens-page.component.css']
})
export class ClientTokensPageComponent implements OnInit {
  displayedColumns = ['accessPoint', 'value', 'actions'];
  datasource = new MatTableDataSource<ClientToken>()

  constructor(
    private clientTokenService: ClientTokenService,
  ) {
    this.clientTokenService.getClientTokens().toPromise().then(v => {
      this.datasource.data = v
    })
  }


  ngOnInit(): void {
  }

  newToken() {
    this.datasource.data.push(new ClientTokenImpl());
    this.datasource.data = this.datasource.data;
  }

  saveToken(clientToken:ClientToken) {
    this.clientTokenService.createClientTokens(clientToken).toPromise().then(v => {
      this.datasource.data[this.datasource.data.indexOf(clientToken)] = v;
      this.datasource.data = this.datasource.data
    })
  }

  remove(u: ClientToken) {
    if(confirm("Are you sure you want to remove the token?")){
      this.clientTokenService.removeClientTokens(u.id).toPromise().then(r => {
        let index = this.datasource.data.indexOf(u)
        this.datasource.data.splice(index, 1)
        this.datasource.data = this.datasource.data;
      })
    }
  }

}
