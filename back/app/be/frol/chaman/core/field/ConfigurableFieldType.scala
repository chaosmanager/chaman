package be.frol.chaman.core.field

import be.frol.chaman.core.data.holder.Holder
import be.frol.chaman.utils.JsonUtils.richJsResult
import be.frol.chaman.utils.OptionUtils.enrichedObject
import play.api.libs.json.{Format, JsNull, JsObject, JsValue}

trait FieldType[T <: Holder]{
  def reference: String
  def inputType: String
  def format: Format[T]
  def configFields: List[ConfigFieldType[_ <: Holder]]


  def formatter: ((T,JsObject) => T) = (v: T, u:JsObject) => v

  def value(v: JsValue): T = v.validate(format).getOrThrow(v.toOpt())

  def parseAndFormat(input: JsValue, config: JsObject): JsValue = {
    if (input == JsNull) JsNull
    else {
      val value = format.reads(input).getOrThrowE(new RuntimeException("Error while reading data"))
      format.writes(formatter(value, config))
    }
  }
}

case class ConfigurableFieldType[T <: Holder](
                                               basicFieldType: BasicFieldType[T],
                                               override val configFields: List[ConfigFieldType[_ <: Holder]]
                       ) extends FieldType[T] {


  override def reference: String = inputType

  override def inputType: String = basicFieldType.inputType

  override def format: Format[T] = basicFieldType.format
}
