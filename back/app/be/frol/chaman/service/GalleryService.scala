package be.frol.chaman.service

import be.frol.chaman.api.DbContext
import be.frol.chaman.tables.Tables
import play.api.db.slick.DatabaseConfigProvider
import slick.util.Logging

import javax.inject.Inject

class GalleryService @Inject()(
                                  val dbConfigProvider: DatabaseConfigProvider,
                                ) extends DbContext with Logging {


  import api._


  def add(p: Tables.ImageGalleryRow) = {
    ((Tables.ImageGallery returning Tables.ImageGallery.map(_.id)
      into ((v, id) => v.copy(id = id))) += p)
  }
  def addAll(p: Seq[Tables.ImageGalleryRow]) = {
    Tables.ImageGallery ++=p
  }

  def delete(p: Tables.ImageGalleryRemovedRow) = {
    Tables.ImageGalleryRemoved += p
  }

  def all(userId:Option[String]) = {
    lastVersion
      .filterOpt(userId)(_.author === _)
      .sortBy(_.id.desc)
      .result
  }

  val lastVersion = {
    Tables.ImageGallery.filterNot(f => Tables.ImageGallery.filter(_.uuid === f.uuid)
      .filter(_.id > f.id).exists)
      .filterNot(f => Tables.ImageGalleryRemoved.filter(f.uuid === _.uuid).exists)
  }


  def find(uuid: String) = lastVersion.filter(_.uuid === uuid).result.head

}
