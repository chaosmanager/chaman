package be.frol.chaman.utils

import com.lowagie.text.{Document, PageSize, RectangleReadOnly}
import org.xhtmlrenderer.pdf.ITextRenderer
import org.xhtmlrenderer.simple.Graphics2DRenderer
import org.xhtmlrenderer.test.Graphics2DRendererTest
import play.api.libs.Files.SingletonTemporaryFileCreator
import play.api.libs.Files.TemporaryFile.temporaryFileToFile
import play.libs.Files.TemporaryFile
import play.twirl.api.Html

import java.io.{ByteArrayOutputStream, FileOutputStream}
import javax.imageio.ImageIO

object PdfUtils {
  def toPdfBytes(v: String) = {
    val os = toPdf(v)
    val r = os.toByteArray
    os.close()
    r
  }

  def toImageBytes(v: String, width:Integer) = {
    val os = toImage(v, width)
    val r = os.toByteArray
    os.close()
    r
  }

  def toPdf(html: String): ByteArrayOutputStream = {

    val renderer = new ITextRenderer()
    val sharedContext = renderer.getSharedContext
    sharedContext.setPrint(true)
    sharedContext.setInteractive(false)
    sharedContext.setReplacedElementFactory(new B64ImgReplacedElementFactory)
    sharedContext.getTextRenderer.setSmoothingThreshold(0)
    renderer.setDocumentFromString(html)
    renderer.layout()
    val os = new ByteArrayOutputStream()
    renderer.createPDF(os)
    os
  }

  def toImage(html: String, width:Int)  = {
    val file = SingletonTemporaryFileCreator.create("chaman", "sticker")
    val stream = new FileOutputStream(temporaryFileToFile(file))
    stream.write(html.getBytes)
    stream.close()

    val image = Graphics2DRenderer.renderToImageAutoSize(file.getPath, width)
    file.delete()
    val os = new ByteArrayOutputStream()
    ImageIO.write(image, "png", os)
    os
  }

}
