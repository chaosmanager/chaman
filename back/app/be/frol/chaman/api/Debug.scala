package be.frol.chaman.api

import be.frol.chaman.utils.RequestUtils
import play.api.libs.json.Json
import play.api.mvc._

import javax.inject.Inject
import scala.concurrent.ExecutionContext

class Debug @Inject()(
                       c: ControllerComponents,
                     )(
                       implicit val ec: ExecutionContext,
                     ) extends AbstractController(c) {


  def debug() = Action { request =>
    Ok(
      Json.toJson(Map(
        "isSecure" -> Json.toJson(RequestUtils.isSecure(request)),
        "headers " -> Json.toJson(request.headers.toMap),
      ))
    )
  }
}
