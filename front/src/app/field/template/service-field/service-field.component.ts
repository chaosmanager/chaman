import {Component, OnInit, ViewChild} from '@angular/core';
import {FieldParent} from "../../core/fieldParent";
import {TagAutocomplete} from "../multiple-tag/tag-autocomplete";
import {MultipleParentComponent} from "../multiple-parent/multiple-parent.component";
import {ServiceFieldService} from "../../../../generated/api";
import {ServicefieldAutocomplete} from "./servicefield-autocomplete";

@Component({
  selector: 'app-service-field',
  templateUrl: './service-field.component.html',
  styleUrls: ['./service-field.component.css']
})
export class ServiceFieldComponent extends FieldParent implements OnInit {

  constructor(
    private serviceFieldService: ServiceFieldService,
  ) {
    super();
  }
d
  autocomplete: ServicefieldAutocomplete

  @ViewChild('multipleParent') multipleParent: MultipleParentComponent<ServiceFieldService>;

  beforeSaveHook() : Promise<void>{
    return this.multipleParent.beforeSaveHook()
  }

  ngOnInit(): void {
    this.autocomplete = new ServicefieldAutocomplete(this.field.uuid, this.serviceFieldService)
  }

}
