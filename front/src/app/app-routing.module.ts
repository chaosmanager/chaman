import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {FieldsComponent} from "./fields/fields/fields.component";
import {ItemCardComponent} from "./item/item-card/item-card.component";
import {ItemSearchComponent} from "./item/item-search/item-search.component";
import {ItemsComponent} from "./item/items/items.component";
import {ScanPageComponent} from "./scan/scan-page/scan-page.component";
import {TokensPageComponent} from "./user/tokens/tokens-page/tokens-page.component";
import {PrintQueueComponent} from "./print/print-queue/print-queue.component";
import {PrintersComponent} from "./print/printers/printers.component";
import {ClientTokenService} from "../generated/api";
import {ClientTokensPageComponent} from "./user/tokens/client-tokens-page/client-tokens-page.component";
import {ImageGalleryPageComponent} from "./image-gallery/image-gallery-page/image-gallery-page.component";


const routes: Routes = [
  {path: 'admin/fields', component: FieldsComponent},
  {path: 'item/:id', component: ItemCardComponent},
  {path: 'user/tokens', component: TokensPageComponent},
  {path: 'user/clientTokens', component: ClientTokensPageComponent},
  {path: 'item', component: ItemsComponent},
  {path: 'scan', component: ScanPageComponent},
  {path: 'printers', component: PrintersComponent},
  {path: 'gallery', component:ImageGalleryPageComponent},
  {path:'', component: ItemsComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
