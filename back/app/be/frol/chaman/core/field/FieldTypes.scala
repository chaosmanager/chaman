package be.frol.chaman.core.field

import be.frol.chaman.core.data.holder.{BooleanHolder, Holder, NumberHolder, StringHolder}
import be.frol.chaman.utils.JsonUtils.richJsResult
import be.frol.chaman.utils.OptionUtils.enrichedObject
import be.frol.chaman.utils.TraversableUtils._
import be.frol.chaman.utils.TryUtils.tryEnriched
import play.api.libs.json._

import scala.util.Try





case class BasicFieldType[T <: Holder](
                                        inputType: String,
                                        format: Format[T],
                                        directValueF: List[T] => JsValue,
                                        override val formatter: ((T,JsObject) => T) = (v: T, u:JsObject) => v,
                                      ) extends FieldType [T]{

  def directValue(v: List[JsValue]) = {
    val values = v.map(v => format.reads(v).getOrThrow(v.toOpt))
    directValueF(values)
  }

  override def reference: String = inputType

  override def configFields: List[ConfigFieldType[_ <: Holder]] = Nil
}

object BasicFieldTypes {
  private def list[T](f: T => Option[JsValue]) = { l: List[T] =>
    JsArray(l.map(f).filterNot(_.isEmpty).map(_.get))
  }

  private def single[T](f: T => Option[JsValue]) = { l: List[T] =>
    l.headOption.flatMap(f(_)).getOrElse(JsNull)
  }

  private def numberFormatter = (v: NumberHolder, config: JsObject) => {
    v.copy(processedValue = v.numericValue, strValue = {
      if(config.value.get(ConfigFieldTypes.numberFormatter.reference) == Some(JsString(ConfigFieldTypes.metric))){
        v.numericValue.map(NumberHolder.toMetric)
      } else {
        v.numericValue.map(_.bigDecimal.toPlainString)
      }
    })
  }

  val sHolderFormat = Json.format[StringHolder]
  val nHolderFormat = Json.format[NumberHolder]
  val bHolderFormat = Json.format[BooleanHolder]

  val multipleTag = BasicFieldType[StringHolder]("multipleTag", sHolderFormat, list(v => v.strValue.map(JsString)))
  val string = BasicFieldType[StringHolder]("string", sHolderFormat, single(v => v.strValue.map(JsString)))
  val multipleString = BasicFieldType[StringHolder]("multipleString", sHolderFormat, list(v => v.strValue.map(JsString)))
  val number = BasicFieldType[NumberHolder]("number", nHolderFormat, single(v => v.numericValue.map(JsNumber(_))), numberFormatter)
  val multipleNumber = BasicFieldType[NumberHolder]("multipleNumber", nHolderFormat, list(v => v.numericValue.map(JsNumber(_))), numberFormatter)
  val select = BasicFieldType[StringHolder]("select", sHolderFormat, single(v => v.strValue.map(JsString)))
  val richText = BasicFieldType[StringHolder]("richText", sHolderFormat, single(v => v.strValue.map(JsString)))
  val checkbox = BasicFieldType[BooleanHolder]("checkbox", bHolderFormat, single(v => v.value.map(JsBoolean)))

}

case class ConfigFieldType[T <: Holder](
                                         uuid: String,
                                         reference: String,
                                         label: String,
                                         basicFieldType: BasicFieldType[T],
                                         config: JsObject = JsObject(Nil)) extends FieldWithConf[T] {
  override def isUserField: Boolean = false

  override def defaultField: Boolean = false

  override def fieldType: ConfigurableFieldType[T] = ConfigurableFieldType(basicFieldType = basicFieldType, configFields = Nil)
}


object ConfigFieldTypes {
  val metric = "metric"
  val suffix = ConfigFieldType[StringHolder]("d889136c-75c1-4020-b89a-37d25b07eda4", "suffix", "Suffix", BasicFieldTypes.string)

  val numberFormatter = ConfigFieldType("057c0520-59de-4a0f-a458-a40c3a6c96f1", "number.formatter", "Number Formatter", BasicFieldTypes.select,
    Json.toJsObject(Map("staticValues" -> List("none", metric, "scientific"))))

  val staticValues = ConfigFieldType("fbcec130-4d2d-420e-a5a3-caea9568828b", "static.values", "Static Values", BasicFieldTypes.multipleString)

  val all: List[ConfigFieldType[_ <: Holder]] = List(suffix, numberFormatter, staticValues)
  val mapUuid = all.toMapBy(_.uuid)
}

object FieldTypes {
  val multipleTag = ConfigurableFieldType(BasicFieldTypes.multipleTag, Nil)
  val string = ConfigurableFieldType(BasicFieldTypes.string, Nil)
  val multipleString = ConfigurableFieldType(BasicFieldTypes.multipleString, Nil)
  val number = ConfigurableFieldType(BasicFieldTypes.number, List(ConfigFieldTypes.suffix, ConfigFieldTypes.numberFormatter))
  val multipleNumber = ConfigurableFieldType(BasicFieldTypes.multipleNumber, List(ConfigFieldTypes.suffix, ConfigFieldTypes.numberFormatter))
  val select = ConfigurableFieldType(BasicFieldTypes.select, List(ConfigFieldTypes.staticValues))
  val richText = ConfigurableFieldType(BasicFieldTypes.richText, Nil)

  val allStatics: List[ConfigurableFieldType[_ <: Holder]] = List(string, multipleString, number, multipleNumber, select, multipleTag, richText)
}
