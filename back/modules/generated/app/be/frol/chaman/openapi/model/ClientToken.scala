package be.frol.chaman.openapi.model

import play.api.libs.json._
import java.time.OffsetDateTime

/**
  * Represents the Swagger definition for ClientToken.
  */
@javax.annotation.Generated(value = Array("org.openapitools.codegen.languages.ScalaPlayFrameworkServerCodegen"))
case class ClientToken(
  id: Option[String],
  date: Option[OffsetDateTime],
  value: Option[String],
  accessPoint: Option[String]
)

object ClientToken {
  implicit lazy val clientTokenJsonFormat: Format[ClientToken] = Json.format[ClientToken]
}

