--

-- !Ups


CREATE TABLE print_job (
                      id bigint(20) NOT NULL AUTO_INCREMENT,
                      uuid varchar(36) NOT NULL,
                      item_uuid varchar(36) NOT NULL,
                      parameters text NULL,
                      author varchar(36) NOT NULL,
                      `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                      PRIMARY KEY (id)
);

CREATE TABLE print_status (
                             id bigint(20) NOT NULL AUTO_INCREMENT,
                             print_id bigint(20) NOT NULL,
                             status varchar(255) NOT NULL,
                             author varchar(36) NOT NULL,
                             `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                             PRIMARY KEY (id)
);
