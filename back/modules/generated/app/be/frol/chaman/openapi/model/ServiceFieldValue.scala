package be.frol.chaman.openapi.model

import play.api.libs.json._

/**
  * Represents the Swagger definition for ServiceFieldValue.
  */
@javax.annotation.Generated(value = Array("org.openapitools.codegen.languages.ScalaPlayFrameworkServerCodegen"))
case class ServiceFieldValue(
  identifier: Option[String],
  value: Option[String]
)

object ServiceFieldValue {
  implicit lazy val serviceFieldValueJsonFormat: Format[ServiceFieldValue] = Json.format[ServiceFieldValue]
}

