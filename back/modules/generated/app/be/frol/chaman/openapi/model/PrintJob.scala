package be.frol.chaman.openapi.model

import play.api.libs.json._

/**
  * Represents the Swagger definition for PrintJob.
  */
@javax.annotation.Generated(value = Array("org.openapitools.codegen.languages.ScalaPlayFrameworkServerCodegen"))
case class PrintJob(
  id: Option[String],
  itemId: Option[String],
  printerId: Option[String],
  status: Option[String]
)

object PrintJob {
  implicit lazy val printJobJsonFormat: Format[PrintJob] = Json.format[PrintJob]
}

