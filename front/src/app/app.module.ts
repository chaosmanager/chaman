import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import {FieldTableComponent} from './fields/field-table/field-table.component';
import {FieldsComponent} from './fields/fields/fields.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {BASE_PATH} from "../generated/api";
import {FieldEditComponent} from './fields/field-edit/field-edit.component';
import {ToastComponent} from './toast/toast.component';
import {FieldComponent} from './field/core/field/field.component';
import {FieldDirective} from "./field/core/field.directive";
import {StringComponent} from './field/template/string/string.component';
import {NumberComponent} from './field/template/number/number.component';
import {WaitComponent} from './wait/wait.component';
import {WaitDirective} from './wait/wait.directive';
import {FieldSelectorComponent} from './fields/field-selector/field-selector.component';
import {FieldValueComponent} from './field/core/field-value/field-value.component';
import {ItemSearchComponent} from './item/item-search/item-search.component';
import {ItemCardComponent} from './item/item-card/item-card.component';
import {ErrorInterceptor} from "./ErrorInterceptor";
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatChipsModule} from "@angular/material/chips";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatIconModule} from "@angular/material/icon";
import {MultipleNumberComponent} from './field/template/multiple-number/multiple-number.component';
import {MultipleStringComponent} from './field/template/multiple-string/multiple-string.component';
import {SelectComponent} from './field/template/select/select.component';
import {MultipleParentComponent} from './field/template/multiple-parent/multiple-parent.component';
import {AnnexTableComponent} from './item/annex/annex-table/annex-table.component';
import {MatTableModule} from "@angular/material/table";
import {MimeToFaPipe} from './mime-to-fa.pipe';
import {ImageCropperComponent} from './image-cropper/image-cropper.component';
import {ThumbnailEditorComponent} from './item/thumbnail-editor/thumbnail-editor.component';
import {CameraSnapshotComponent} from "./camera-input/camera-snapshot/camera-snapshot.component";
import {ThumbnailCameraComponent} from './item/thumbnail-camera/thumbnail-camera.component';
import {MultipleTagComponent} from './field/template/multiple-tag/multiple-tag.component';
import {MatAutocompleteModule} from "@angular/material/autocomplete";
import {ItemSelectorModalComponent} from './item/item-selector-modal/item-selector-modal.component';
import {LinkTableComponent} from './item/link/link-table/link-table.component';
import {ItemsComponent} from './item/items/items.component';
import {FieldSavableComponent} from './field/field-savable/field-savable.component';
import {MatInputModule} from "@angular/material/input";
import {ScanModalComponent} from './scan/scan-modal/scan-modal.component';
import {ZXingScannerModule} from "@zxing/ngx-scanner";
import {ScanPageComponent} from './scan/scan-page/scan-page.component';
import {DescCardComponent} from './item/desc-card/desc-card.component';
import {MoveModalComponent} from './item/move-modal/move-modal.component';
import {TokensPageComponent} from './user/tokens/tokens-page/tokens-page.component';
import {PrintQueueComponent} from './print/print-queue/print-queue.component';
import {TruncatePipe} from './truncate.pipe';
import {PrintersComponent} from './print/printers/printers.component';
import {RichTextComponent} from './field/template/rich-text/rich-text.component';
import {QuillModule} from "ngx-quill";
import { ServiceFieldComponent } from './field/template/service-field/service-field.component';
import {MatProgressBarModule} from "@angular/material/progress-bar";
import { ItemLinkComponent } from './item/item-link/item-link.component';
import { ClientTokensPageComponent } from './user/tokens/client-tokens-page/client-tokens-page.component';
import { ImageGalleryPageComponent } from './image-gallery/image-gallery-page/image-gallery-page.component';
import { ImageGalleryComponent } from './image-gallery/image-gallery/image-gallery.component';
import {MatPaginatorModule} from "@angular/material/paginator";

const quill_config = {
  toolbar: [
    ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
    ['blockquote', 'code-block'],

    [{'header': 1}, {'header': 2}],               // custom button values
    [{'list': 'ordered'}, {'list': 'bullet'}],
    [{'indent': '-1'}, {'indent': '+1'}],          // outdent/indent

    [{'size': ['small', false, 'large', 'huge']}],  // custom dropdown
    [{'header': [1, 2, 3, 4, 5, 6, false]}],

    [{'color': []}, {'background': []}],          // dropdown with defaults from theme
    [{'font': []}],
    [{'align': []}],

    ['clean'],                                         // remove formatting button
    ['link'],
  ]
};


@NgModule({
  declarations: [
    AppComponent,
    FieldTableComponent,
    FieldsComponent,
    FieldEditComponent,
    ToastComponent,
    FieldComponent,
    FieldDirective,
    StringComponent,
    NumberComponent,
    WaitComponent,
    WaitDirective,
    FieldSelectorComponent,
    FieldValueComponent,
    ItemSearchComponent,
    ItemCardComponent,
    MultipleNumberComponent,
    MultipleStringComponent,
    SelectComponent,
    MultipleParentComponent,
    AnnexTableComponent,
    MimeToFaPipe,
    ImageCropperComponent,
    ThumbnailEditorComponent,
    CameraSnapshotComponent,
    ThumbnailCameraComponent,
    MultipleTagComponent,
    ItemSelectorModalComponent,
    LinkTableComponent,
    ItemsComponent,
    FieldSavableComponent,
    ScanModalComponent,
    ScanPageComponent,
    DescCardComponent,
    MoveModalComponent,
    TokensPageComponent,
    PrintQueueComponent,
    TruncatePipe,
    PrintersComponent,
    RichTextComponent,
    ServiceFieldComponent,
    ItemLinkComponent,
    ClientTokensPageComponent,
    ImageGalleryPageComponent,
    ImageGalleryComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    NgbModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatAutocompleteModule,
    MatChipsModule,
    MatFormFieldModule,
    MatIconModule,
    ReactiveFormsModule,
    MatTableModule,
    MatInputModule,
    ZXingScannerModule,
    QuillModule.forRoot(),
    MatProgressBarModule,
    MatPaginatorModule,

  ],
  providers: [
    {provide: BASE_PATH, useValue: '/api'},
    {provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true},
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
