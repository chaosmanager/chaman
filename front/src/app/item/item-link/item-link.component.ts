import {Component, Input, OnInit} from '@angular/core';
import {Item} from "../../../generated/api";

@Component({
  selector: 'app-item-link',
  templateUrl: './item-link.component.html',
  styleUrls: ['./item-link.component.css']
})
export class ItemLinkComponent implements OnInit {

  constructor() {
  }

  @Input() item: Item;
  @Input() link: boolean = true;

  ngOnInit(): void {
  }

}
