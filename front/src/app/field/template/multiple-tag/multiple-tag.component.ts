import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FieldParent} from "../../core/fieldParent";
import {TagService} from "../../../../generated/api";
import {Observable} from "rxjs";
import {map} from "rxjs/operators";
import {TagAutocomplete, TagValue} from "./tag-autocomplete";
import {MultipleParentComponent} from "../multiple-parent/multiple-parent.component";


@Component({
  selector: 'app-multiple-tag',
  templateUrl: './multiple-tag.component.html',
  styleUrls: ['./multiple-tag.component.css']
})
export class MultipleTagComponent extends FieldParent implements OnInit {

  constructor(
    private tagService: TagService,
  ) {
    super();
  }

  autocomplete: TagAutocomplete

  @ViewChild('multipleParent') multipleParent: MultipleParentComponent<TagValue>;

  beforeSaveHook() : Promise<void>{
    return this.multipleParent.beforeSaveHook()
  }

  ngOnInit(): void {
    this.autocomplete = new TagAutocomplete(this.field.uuid, this.tagService)
  }

}
