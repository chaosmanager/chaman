import { NgModule, ModuleWithProviders, SkipSelf, Optional } from '@angular/core';
import { Configuration } from './configuration';
import { HttpClient } from '@angular/common/http';

import { AnnexService } from './api/annex.service';
import { ClientTokenService } from './api/clientToken.service';
import { CopyItemService } from './api/copyItem.service';
import { DatatypesService } from './api/datatypes.service';
import { EventService } from './api/event.service';
import { FieldService } from './api/field.service';
import { FileService } from './api/file.service';
import { GalleryService } from './api/gallery.service';
import { ImportService } from './api/import.service';
import { ItemService } from './api/item.service';
import { LinkService } from './api/link.service';
import { PrintService } from './api/print.service';
import { ScannerService } from './api/scanner.service';
import { ServiceFieldService } from './api/serviceField.service';
import { StickerTemplateService } from './api/stickerTemplate.service';
import { TagService } from './api/tag.service';
import { ThumbnailService } from './api/thumbnail.service';
import { UserService } from './api/user.service';
import { UserTokenService } from './api/userToken.service';

@NgModule({
  imports:      [],
  declarations: [],
  exports:      [],
  providers: []
})
export class ApiModule {
    public static forRoot(configurationFactory: () => Configuration): ModuleWithProviders<ApiModule> {
        return {
            ngModule: ApiModule,
            providers: [ { provide: Configuration, useFactory: configurationFactory } ]
        };
    }

    constructor( @Optional() @SkipSelf() parentModule: ApiModule,
                 @Optional() http: HttpClient) {
        if (parentModule) {
            throw new Error('ApiModule is already loaded. Import in your base AppModule only.');
        }
        if (!http) {
            throw new Error('You need to import the HttpClientModule in your AppModule! \n' +
            'See also https://github.com/angular/angular/issues/20575');
        }
    }
}
