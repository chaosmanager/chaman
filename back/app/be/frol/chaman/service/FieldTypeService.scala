package be.frol.chaman.service

import be.frol.chaman.Conf
import be.frol.chaman.api.DbContext
import be.frol.chaman.core.data.holder.Holder
import be.frol.chaman.core.field.FieldTypes.allStatics
import be.frol.chaman.core.field.{ConfigurableFieldType, DefaultFields, FieldType, FieldTypes, FieldWithConf, StaticFields}
import be.frol.chaman.mapper.FieldMapper
import be.frol.chaman.model.RichModelConversions._
import be.frol.chaman.model.UserInfo
import be.frol.chaman.tables.Tables
import be.frol.chaman.tables.Tables.FieldRow
import be.frol.chaman.utils.DateUtils
import be.frol.chaman.utils.OptionUtils.enrichedOption
import be.frol.chaman.utils.TraversableUtils.traversableEnriched
import play.api.{Configuration, Logging}
import play.api.db.slick.DatabaseConfigProvider

import javax.inject.Inject
import scala.concurrent.ExecutionContext

class FieldTypeService @Inject()(
                              val dbConfigProvider: DatabaseConfigProvider,
                            )(
                              implicit executionContext: ExecutionContext,
                              val conf:Configuration,
                            ) extends DbContext with Logging{

  lazy val allTypes :List[FieldType[_ <: Holder]] = FieldTypes.allStatics ++ Conf.serviceFields

  val mapByInputType: Map[String, FieldType[_ <: Holder]] = allTypes.toMapBy(_.reference)

  def get(dataType: String) = mapByInputType.get(dataType)
}

