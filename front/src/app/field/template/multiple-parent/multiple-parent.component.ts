import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {MatChipInputEvent} from "@angular/material/chips";
import {FieldValueImpl} from "../../../model/FieldValueImpl";
import {Field, FieldValue} from "../../../../generated/api";
import {
  MatAutocompleteActivatedEvent,
  MatAutocompleteSelectedEvent,
  MatAutocompleteTrigger
} from "@angular/material/autocomplete";
import {FormControl} from "@angular/forms";
import {Observable} from "rxjs";
import {map, mergeMap, startWith} from "rxjs/operators";
import {Autocomplete} from "./autocomplete";
import {NgbModal, NgbModalConfig} from "@ng-bootstrap/ng-bootstrap";
import {ScanModalComponent} from "../../../scan/scan-modal/scan-modal.component";
import {of} from "rxjs/internal/observable/of";

@Component({
  selector: 'app-multiple-parent',
  templateUrl: './multiple-parent.component.html',
  styleUrls: ['./multiple-parent.component.css']
})
export class MultipleParentComponent<T> implements OnInit {
  private _valueInput: ElementRef<HTMLInputElement>;

  constructor(
    private modalService: NgbModal,
    private modalConfig: NgbModalConfig,
  ) {
    this.modalConfig.size = 'xl'
  }

  activatedOption: string

  filteredInputs: Observable<string[]>;

  @ViewChild('valueInput', {read: MatAutocompleteTrigger}) matAutoComplete: MatAutocompleteTrigger

  @ViewChild('valueInput') set valueInput(v: ElementRef<HTMLInputElement>) {
    this._valueInput = v;
    if (v) {
      v.nativeElement.focus();
    }
  };

  get valueInput() {
    return this._valueInput;
  }

  @Input() field: Field;
  @Input() autoComplete: Autocomplete<T>

  _inEdit: boolean = false;

  @Input() set inEdit(v: boolean) {
    this._inEdit = v;
  }

  get inEdit() {
    return this._inEdit;
  }


  inputCtrl = new FormControl();


  ngOnInit(): void {
    if (this.autoComplete) {
      this.filteredInputs = this.inputCtrl.valueChanges.pipe(
        startWith(null),
        mergeMap((v: string | null) => this._filter(v)));
    }
  }

  addValueWithEvent(s: MatChipInputEvent) {
    let value = s.value;
    this.addValue(value)

  }

  private addValue(value: string): Promise<void> {
    if (value != "") {
      if (!this.field.value) {
        this.field.value = []
      }
      if (this.autoComplete) {
        this.addAutocomplete(value);
      } else {
        this.add(value)
        this.valueInput.nativeElement.value = "";
        return Promise.resolve()
      }
    }
  }

  private addAutocomplete(value: string) {
    this.autoComplete.get(value).toPromise().then(v => {
        if (v) {
          this.field.value.push(new FieldValueImpl(null,v))
          this.cleanField()
        } else if (this.autoComplete.canAdd()) {
          this.autoComplete.add(value).toPromise()
            .then(added => {
                if (added) {
                  this.field.value.push(new FieldValueImpl(null,added))
                  this.cleanField();
                }
              }
            )
        }
      }
    )
  }

  private cleanField() {
    this.valueInput.nativeElement.value = "";
  }

  private add(s: string) {
    this.field.value.push(new FieldValueImpl(null, {'strValue': s}))
  }


  remove(v: FieldValue) {
    this.field.value.splice(this.field.value.indexOf(v), 1)
  }


  selected(event: MatAutocompleteSelectedEvent): void {
    this.cleanField()
    this.addValue(event.option.viewValue)
  }

  private _filter(v: string): Observable<string[]> {
    return this.autoComplete.getSuggestions(v).pipe(
      map(l => l.map(v => this.autoComplete.stringRepr(v)))
    )
  }

  public beforeSaveHook(): Promise<void> {
    if (this.valueInput.nativeElement.value && this.valueInput.nativeElement.value !== "") {
      return this.addValue(this.valueInput.nativeElement.value)
    } else {
      return Promise.resolve();
    }
  }

  optionActivated(event: MatAutocompleteActivatedEvent) {
    this.activatedOption = event?.option?.value
  }

  partialComplete() {
    if (this.activatedOption) {
      const nextPoint = this.activatedOption.indexOf('.', this.valueInput.nativeElement.value.length)
      if (nextPoint === -1) {
        this.valueInput.nativeElement.value = this.activatedOption
      } else {
        let value = this.activatedOption.slice(0, nextPoint);
        this.valueInput.nativeElement.value = value + '.'
        this.matAutoComplete.openPanel()
      }

    }
    return false
  }

  scan() {
    const ref = this.modalService.open(ScanModalComponent)
    ref.componentInstance.scanned.subscribe(v => this.addValue(v))
  }

  isLink(strValue: string) {
    return strValue.startsWith("https://") || strValue.startsWith("http://")
  }
}
