package be.frol.chaman.openapi.api

import org.openapitools.OpenApiExceptions
import javax.inject.{Inject, Singleton}
import play.api.libs.json._
import play.api.mvc._
import scala.concurrent.{ExecutionContext, Future}
import be.frol.chaman.openapi.model.StickerTemplate

@javax.annotation.Generated(value = Array("org.openapitools.codegen.languages.ScalaPlayFrameworkServerCodegen"))
@Singleton
class StickerTemplateApiController @Inject()(cc: ControllerComponents, api: StickerTemplateApi)(implicit executionContext: ExecutionContext) extends AbstractController(cc) {
  /**
    * DELETE /api/sticker-template/:uuid
    */
  def deleteStickerTemplate(uuid: String): Action[AnyContent] = Action.async { request =>
    def executeApi(): Future[Unit] = {
      api.deleteStickerTemplate(uuid)(request)
    }

    executeApi().map { _ =>
      Ok
    }
  }

  /**
    * GET /api/sticker-template/:uuid
    */
  def getStickerTemplate(uuid: String): Action[AnyContent] = Action.async { request =>
    def executeApi(): Future[StickerTemplate] = {
      api.getStickerTemplate(uuid)(request)
    }

    executeApi().map { result =>
      val json = Json.toJson(result)
      Ok(json)
    }
  }

  /**
    * PUT /api/sticker-template/:uuid
    */
  def putStickerTemplate(uuid: String): Action[AnyContent] = Action.async { request =>
    def executeApi(): Future[StickerTemplate] = {
      val stickerTemplate = request.body.asJson.map(_.as[StickerTemplate])
      api.putStickerTemplate(uuid, stickerTemplate)(request)
    }

    executeApi().map { result =>
      val json = Json.toJson(result)
      Ok(json)
    }
  }

  private def splitCollectionParam(paramValues: String, collectionFormat: String): List[String] = {
    val splitBy =
      collectionFormat match {
        case "csv" => ",+"
        case "tsv" => "\t+"
        case "ssv" => " +"
        case "pipes" => "|+"
      }

    paramValues.split(splitBy).toList
  }
}
