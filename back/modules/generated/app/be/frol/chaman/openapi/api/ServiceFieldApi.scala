package be.frol.chaman.openapi.api

import play.api.libs.json._
import play.api.mvc._
import scala.concurrent.Future
import be.frol.chaman.openapi.model.ServiceFieldValue

@javax.annotation.Generated(value = Array("org.openapitools.codegen.languages.ScalaPlayFrameworkServerCodegen"))
trait ServiceFieldApi {
  /**
    * get values list for a field
    */
  def getAllValues(uuid: String)(implicit request:Request[AnyContent]): Future[List[ServiceFieldValue]]

}
