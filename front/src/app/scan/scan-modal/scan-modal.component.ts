import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {NgbActiveModal, NgbModal, NgbModalRef} from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'app-scan-modal',
  templateUrl: './scan-modal.component.html',
  styleUrls: ['./scan-modal.component.css']
})
export class ScanModalComponent implements OnInit {

  constructor(
    public modal:NgbActiveModal,
  ) { }

  scannedValues = []

  @Output() scanned = new EventEmitter<string>()

  ngOnInit(): void {
  }

  emit(v){
    this.scanned.emit(v)
    this.scannedValues.splice(this.scannedValues.indexOf(v),1)
  }

  addScan(result) {
    if(this.scannedValues.indexOf(result) === -1){
      this.scannedValues.push(result)
    }
  }


}
