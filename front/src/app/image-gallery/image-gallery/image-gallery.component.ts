import {AfterViewInit, Component, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {Annex, GalleryService} from "../../../generated/api";
import {MatTableDataSource} from "@angular/material/table";
import {MatPaginator} from "@angular/material/paginator";
import {BehaviorSubject} from "rxjs";

@Component({
  selector: 'app-image-gallery',
  templateUrl: './image-gallery.component.html',
  styleUrls: ['./image-gallery.component.css']
})
export class ImageGalleryComponent implements OnInit, AfterViewInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;

  @Output() selection= new EventEmitter<Annex>()

  ngAfterViewInit() {
    this.images.paginator = this.paginator;
  }
  images = new MatTableDataSource<Annex>()
  displayedColumns = ['image', 'name']
  constructor(
    public galleryService:GalleryService,
  ) { }

  ngOnInit(): void {
    this.refresh()
  }

  public refresh(){
    this.galleryService.getImages()
      .subscribe(i => this.images.data = i)
  }

  select(f) {
    this.selection.emit(f)
  }
}
