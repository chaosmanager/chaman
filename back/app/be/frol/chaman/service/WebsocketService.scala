package be.frol.chaman.service

import akka.actor.ActorSystem
import be.frol.chaman.Conf
import be.frol.chaman.api.DbContext
import be.frol.chaman.model.RichModelConversions._
import be.frol.chaman.model.UserInfo
import be.frol.chaman.openapi.model.Event
import be.frol.chaman.tables.Tables
import be.frol.chaman.tables.Tables.{DataDeletedRow, DataRow}
import be.frol.chaman.utils.DateUtils
import be.frol.chaman.utils.TraversableUtils.traversableEnriched
import play.api.Configuration
import play.api.db.slick.DatabaseConfigProvider
import play.api.libs.json.Json
import play.api.mvc.{AnyContent, Request}
import redis.RedisClient
import slick.util.Logging

import javax.inject.Inject
import scala.concurrent.{ExecutionContext, Future}

class WebsocketService @Inject()(
                           )(
                             implicit val executionContext: ExecutionContext,
                             val conf: Configuration,
                             val actorSystem: ActorSystem,
                           ) extends Logging {


  val redisClient = RedisClient(Conf.redisHost)

  def addUserEvent(event: Event)(implicit user: UserInfo): Future[Long] = {
    addEvent(event, user.uuid)
  }

  def addEvent(event: Event, uuid:String): Future[Long] = {
    val eventStr = Json.toJson(event).toString()
    redisClient.publish(uuid, eventStr)
  }

}
