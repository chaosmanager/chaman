package be.frol.chaman.openapi.api

import play.api.libs.json._
import play.api.mvc._
import scala.concurrent.Future
import be.frol.chaman.openapi.model.UserToken

@javax.annotation.Generated(value = Array("org.openapitools.codegen.languages.ScalaPlayFrameworkServerCodegen"))
trait UserTokenApi {
  /**
    * create user token
    */
  def createUserTokens()(implicit request:Request[AnyContent]): Future[UserToken]


  /**
    * get user tokens
    */
  def getUserTokens()(implicit request:Request[AnyContent]): Future[List[UserToken]]


  /**
    * remove tokens
    */
  def removeUserTokens(uuid: String)(implicit request:Request[AnyContent]): Future[Unit]

}
