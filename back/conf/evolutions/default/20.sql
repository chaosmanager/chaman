-- Annex

-- !Ups

CREATE TABLE image_gallery (
                       id bigint(20) NOT NULL AUTO_INCREMENT,
                       uuid varchar(36) NOT NULL,
                       fileSha varchar(40) NOT NULL,
                       name varchar(255) NOT NULL,
                       mimeType varchar(255) NOT NULL,
                       author varchar(36) NOT NULL,
                       `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                       PRIMARY KEY (id)
);

create index uuid_index on image_gallery(uuid);

create table image_gallery_removed (
                               uuid varchar(36) NOT NULL REFERENCES image_gallery(uuid),
                               author varchar(36) NOT NULL,
                               `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                               PRIMARY KEY (uuid)
);

