#!/bin/sh

set -ue

DIR=$(cd $(dirname $0) && pwd)
PROJECT_DIR=$DIR/..
INFO_COLOR='\033[0;33m'
ERROR_COLOR='\033[0;3om'
NC='\033[0m' # No Color

info(){
  echo -e ${INFO_COLOR}$@${NC}
}

error(){
  >&2 echo -e ${ERROR_COLOR}$@${NC}
}

IMAGE_PREFIX=$1
IMAGE_TAG=$2

info build with image prefix $IMAGE_PREFIX and tag $IMAGE_TAG

info check table have no schema
grep -R 'Some("chaman")' $PROJECT_DIR/back/app/be/frol/chaman/tables/Tables.scala >/dev/null && (error "the schema should not be defined in Tables.scala"; exit 1)


info build front
docker-compose -f $DIR/../dev-env/docker-compose.yml run --rm --entrypoint npm front --force install
docker-compose -f $DIR/../dev-env/docker-compose.yml run --rm front ng build --prod
rm -rf $DIR/tmp || echo ""
mkdir -p  $DIR/../back/public/
cp -r $DIR/../front/dist/chaman/* $DIR/../back/public/

info build back
docker-compose -f $DIR/../dev-env/docker-compose.yml run --rm back "clean; dist"
rm -rf $DIR/tmp || echo ""
mkdir -p $DIR/tmp
unzip $DIR/../back/target/universal/back-*.zip -d $DIR/tmp
mv $DIR/tmp/back-* $DIR/tmp/back
cp $DIR/Dockerfile.back $DIR/tmp/Dockerfile
cp $DIR/resources/back/* $DIR/tmp/
docker build -t $IMAGE_PREFIX/chaman$IMAGE_TAG $DIR/tmp
