import {Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {Field, ItemService} from "../../../generated/api";
import {RxjsHelperService} from "../../rxjs-helper.service";
import {MultipleParentComponent} from "../template/multiple-parent/multiple-parent.component";
import {FieldParent} from "../core/fieldParent";
import {FieldComponent} from "../core/field/field.component";

@Component({
  selector: 'app-field-savable',
  templateUrl: './field-savable.component.html',
  styleUrls: ['./field-savable.component.css']
})
export class FieldSavableComponent implements OnInit {

  constructor(
    private r: RxjsHelperService,
    private itemService: ItemService,
  ) {
  }

  @Input() showLabel = true
  @Input() itemUuid: string
  @Input() subReference: string
  @Input() field: Field
  @Input() inEdit: boolean
  @Output() onDelete = new EventEmitter<Field>()

  @ViewChild('fieldElement') fieldElement: FieldComponent;

  ngOnInit(): void {
  }

  save() {
    this.fieldElement.beforeSaveHook().then(v => {
      this.r.wrap(this.itemService.updateItemField(this.itemUuid, this.field.uuid, this.field, this.subReference))
        .withErrorMessage("Failed to update field")
        .withSuccessMessage("Field updated")
        .then(v => {
          this.inEdit=false
          this.field = v
        }, v => this.field.errorMessages = v.error.content.errorMessages)
    })

  }

  delete() {
    this.r.wrap(this.itemService.deleteItemField(this.itemUuid, this.field.uuid))
      .withErrorMessage("Failed to remove field")
      .withSuccessMessage("Field removed")
      .then(v => this.onDelete.emit(this.field))
  }

  startEdit() {
    this.inEdit = true;
  }
}
