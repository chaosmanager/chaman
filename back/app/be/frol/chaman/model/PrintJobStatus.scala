package be.frol.chaman.model

object PrintJobStatus extends Enumeration {
  type PrintJobStatus = Value
  val ToPrint, Printed, Cancelled = Value
}
