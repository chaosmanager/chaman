package be.frol.chaman.utils

import be.frol.chaman.Conf
import play.api.libs.Files

import javax.xml.bind.DatatypeConverter

object FileUtils {

  def addFileToAnnex(f: Files.TemporaryFile): String = {
    val sha1Sum = DatatypeConverter.printHexBinary(
      java.security.MessageDigest.getInstance("SHA-1").digest(
        java.nio.file.Files.readAllBytes(f)))

    if (!java.nio.file.Files.exists(Conf.annex(sha1Sum).toPath)) {
      java.nio.file.Files.move(f, Conf.annex(sha1Sum).toPath)
    }
    sha1Sum
  }

}
