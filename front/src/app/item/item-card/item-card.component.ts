import {Component, ElementRef, OnDestroy, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {
  Annex,
  AnnexService,
  CopyItemService,
  EventService,
  Field,
  Item,
  ItemDescr,
  ItemService,
  Link,
  LinkService,
  PrintService
} from "../../../generated/api";
import {ItemImpl} from "../../model/ItemImpl";
import {RxjsHelperService} from "../../rxjs-helper.service";
import {NgbModal, NgbModalConfig} from "@ng-bootstrap/ng-bootstrap";
import {FieldSelectorComponent} from "../../fields/field-selector/field-selector.component";
import {AnnexTableComponent} from "../annex/annex-table/annex-table.component";
import {ThumbnailEditorComponent} from "../thumbnail-editor/thumbnail-editor.component";
import {BehaviorSubject} from "rxjs";
import {MobileDetectorService} from "../../mobile-detector.service";
import {ThumbnailCameraComponent} from "../thumbnail-camera/thumbnail-camera.component";
import {ItemSelectorModalComponent} from "../item-selector-modal/item-selector-modal.component";
import {LinkTableComponent} from "../link/link-table/link-table.component";
import {Location} from "@angular/common";
import {ContextService} from "../../context.service";
import {MoveModalComponent} from "../move-modal/move-modal.component";
import {HttpEvent, HttpEventType} from "@angular/common/http";
import {ToastService} from "../../toast/toast.service";

@Component({
  selector: 'app-item-card',
  templateUrl: './item-card.component.html',
  styleUrls: ['./item-card.component.css']
})
export class ItemCardComponent implements OnInit, OnDestroy {

  item: Item
  progress = undefined;

  constructor(
    public route: ActivatedRoute,
    private itemService: ItemService,
    private r: RxjsHelperService,
    private modalService: NgbModal,
    private modalConfig: NgbModalConfig,
    private annexService: AnnexService,
    private eventService: EventService,
    public mobileService: MobileDetectorService,
    public location: Location,
    public linkService: LinkService,
    public copyItemService: CopyItemService,
    private printService: PrintService,
    public context: ContextService,
    private toastService: ToastService,
  ) {
    route.params.subscribe(p => {
        if (p.id === 'new') {
          this.item = new ItemImpl()
        } else {
          this.context.itemContext.next({uuid: p.id})
          itemService.getItem(p.id).toPromise().then(i => {
            this.context.itemContext.next(i)
            this.item = i
            this.refreshThumbnail()
          })
        }

      }
    )
    this.modalConfig.size = 'xl'
  }

  ngOnDestroy(): void {
    this.context.itemContext.next(undefined)
  }

  imgSrc: BehaviorSubject<string> = new BehaviorSubject(undefined);

  @ViewChild("annexTable") annexTable: AnnexTableComponent;
  @ViewChild("linkTable") linkTable: LinkTableComponent;
  @ViewChild("holderTable") holderTable: LinkTableComponent;
  @ViewChild("holdingTable") holdingTable: LinkTableComponent;
  @ViewChild('annexByUrl') annexByUrlInput: ElementRef<HTMLInputElement>;

  ngOnInit(): void {

  }


  startAddField() {
    if (!this.item.content) this.item.content = []
    let open = this.modalService.open(FieldSelectorComponent);
    (open.componentInstance as FieldSelectorComponent).selectField.asObservable().subscribe(v =>
      this.item.content.push(v)
    )
  }

  deleteField(field: Field) {
    this.item.content.splice(this.item.content.indexOf(field), 1)
  }

  showSticker(tpl) {
    this.modalService.open(tpl)
  }

  copy() {
    this.r.wrap(this.copyItemService.copyItem(this.item.uuid))
      .withErrorMessage("Couldn't copy item")
      .withSuccessMessage("Item copied")
      .then(v =>
        window.location.href = "/item/" + v.uuid
      )
  }

  onFileChange(event: any) {
    this.uploadFile(event.target.files[0]);
  }

  private uploadFile(file: any) {
    this.annexService.uploadFile(this.item.uuid, file, 'events', true)
      .subscribe((event: HttpEvent<any>) => {
        switch (event.type) {
          case HttpEventType.ResponseHeader:
            if(event.status < 300 && event.status >= 200) {
              this.toastService.showSuccess("Annex added")
            } else {
              this.toastService.showError("Error while uploading annex", null)
            }
            this.progress = 0;
            break;
          case HttpEventType.UploadProgress:
            var eventTotal = event.total ? event.total : 0;
            this.progress = Math.round(event.loaded / eventTotal * 100);
            break;
          case HttpEventType.Response:
            this.item.annexes.push(event.body)
            this.annexTable.refresh();
            this.refreshThumbnail();
            setTimeout(() => {
              this.progress = 0;
            }, 1500);
        }
      })
  }

  annexDeleted(a: Annex) {
    this.item.annexes.splice(this.item.annexes.indexOf(a), 1)
    this.annexTable.refresh()
  }


  refreshThumbnail() {
    this.imgSrc.next("/api/item/" + this.item.uuid + "/thumbnail/file?ts=" + Date.now())
  }

  editThumbnail() {
    let ngbModalRef = this.modalService.open(ThumbnailEditorComponent);
    ngbModalRef.componentInstance.item = this.item;
    ngbModalRef.result.then(v => this.refreshThumbnail())
  }

  takePhoto() {
    let ngbModalRef = this.modalService.open(ThumbnailCameraComponent);
    ngbModalRef.componentInstance.item = this.item;
    ngbModalRef.result.then(v => this.refreshThumbnail())
  }


  addLink(isHoldingRelation: boolean = false, isHolderRelation: boolean = false) {
    if (!this.item.links) this.item.links = []
    this.modalService.open(ItemSelectorModalComponent).result.then((result) => {
      this.linkService.addLink(this.item.uuid, result.uuid, isHoldingRelation, isHolderRelation).toPromise().then(r => {
        this.item.links.push(r)
        new Array(this.linkTable, this.holderTable, this.holdingTable).forEach(v => v.refresh())
      })
    });
  }

  addHolder() {
    this.addLink(false, true)
  }

  addHolding() {
    let ngbModalRef = this.modalService.open(MoveModalComponent);
    (ngbModalRef.componentInstance as MoveModalComponent).itemDestination = this.item as ItemDescr
    (ngbModalRef.componentInstance as MoveModalComponent).moveMultipleItems = true

  }


  linkDeleted(l: Link) {
    this.item.links.splice(this.item.links.indexOf(l), 1)
    this.linkTable.refresh()
    this.holderTable.refresh()
    this.holdingTable.refresh()
  }

  onPaste(e: ClipboardEvent) {
    const items: any = e.clipboardData.items;
    let blob = null;
    for (const item of items) {
      if (item.type.indexOf('image') === 0) {
        this.uploadFile(item.getAsFile());
      }
    }
  }

  addAnnexFromUrl() {
    const value = this.annexByUrlInput.nativeElement.value
    this.annexByUrlInput.nativeElement.value = "";
    this.r.wrap(this.annexService.addByUrl(this.item.uuid, value))
      .withErrorMessage("Error while adding annex")
      .withSuccessMessage("Annex added")
      .then(v => {
        this.item.annexes.push(v)
        this.annexTable.refresh();
        this.refreshThumbnail();
      })

  }

  get imgLocation() {
    return "/api/item/" + this.item.uuid + "/stickerImg?ts=" + Date.now();
  }

  print() {
    this.r.wrap(this.printService.addPrint(this.context.printers[0].id, this.item.uuid))
      .withSuccessMessage("Print request added")
      .withErrorMessage("Error while sending print request")
      .then(v => console.log("send print request", v))
  }

  addImageFromGallery(f:Annex) {
    this.r.wrap(this.annexService.addAnnexFromGallery(this.item.uuid, f.uuid))
      .withErrorMessage("Error while adding annex")
      .withSuccessMessage("Annex added")
      .then(v => {
        this.item.annexes.push(v)
        this.annexTable.refresh();
        this.refreshThumbnail();
        this.modalService.dismissAll()
      })
  }

  selectFromGallery(gallery: TemplateRef<any>) {
    this.modalService.open(gallery)
  }
}
