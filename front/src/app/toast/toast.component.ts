import {Component, OnInit, TemplateRef} from '@angular/core';
import {ToastService} from "./toast.service";
import {ItemDescr} from "../../generated/api";

@Component({
  selector: 'app-toasts',
  template: `
    <ngb-toast
      *ngFor="let toast of toastService.toasts"
      [class]="toast.classname"
      [autohide]="true"
      [delay]="toast.delay || 100000"
      (hidden)="toastService.remove(toast)"
    >
      <ng-template [ngIf]="isTemplate(toast)">
        <ng-template [ngTemplateOutlet]="toast.content"></ng-template>
      </ng-template>

      <ng-template [ngIf]="isItem(toast)"><app-desc-card [item]="toast.content"></app-desc-card></ng-template>

      <ng-template [ngIf]="isText(toast)">{{ toast.content }}</ng-template>
    </ngb-toast>
  `,
  host: {'[class.ngb-toasts]': 'true'}
})
export class ToastComponent {
  constructor(public toastService: ToastService) {}

  isTemplate(toast) { return toast.content instanceof TemplateRef; }

  isItem(toast) { return toast.content.uuid; }

  isText(toast) { return !this.isTemplate(toast) && !this.isItem(toast)}
}
