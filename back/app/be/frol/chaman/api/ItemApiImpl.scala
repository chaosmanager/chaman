package be.frol.chaman.api

import be.frol.chaman.mapper.{FieldMapper, ItemMapper}
import be.frol.chaman.openapi.api.ItemApi
import be.frol.chaman.openapi.model.{Field, Item, ItemDescr}
import be.frol.chaman.service._
import be.frol.chaman.tables
import be.frol.chaman.utils.DateUtils
import play.api.db.slick.DatabaseConfigProvider
import play.api.mvc.{AnyContent, ControllerComponents, Request}

import java.util.UUID
import javax.inject.Inject
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future


class ItemApiImpl @Inject()(
                             val cc: ControllerComponents,
                             val dbConfigProvider: DatabaseConfigProvider,
                             itemService: ItemService,
                             fieldDataService: DataService,
                             fieldValidationService: FieldValidationService,
                             itemRefresher: ItemRefresherService,
                             itemGetterService: ItemGetterService,
                           ) extends ItemApi with DbContext with ParentController {

  import api._

  override def createItem()(implicit request: Request[AnyContent]): Future[ItemDescr] = run { implicit u =>
     Future(new tables.Tables.ItemRow(0L, UUID.randomUUID().toString, "", "", u.uuid, DateUtils.ts))
      .map(ItemMapper.toDescrDto(_))
  }

  override def deleteItem(uuid: String)(implicit request: Request[AnyContent]): Future[Unit] = run { implicit u =>
    db.run(itemService.delete(uuid)).map(_ => None)
  }

  override def getItem(uuid: String, refresh: Option[Boolean])(implicit request: Request[AnyContent]): Future[Item] = run { implicit u =>
    db.run(
      (if (refresh.getOrElse(false)) itemRefresher.refresh(uuid) else DBIO.successful(None)).flatMap(_ =>
        itemGetterService.getSavedItem(uuid))
    )
  }

  override def getItemDescr(uuid: String)(implicit request: Request[AnyContent]): Future[ItemDescr] = {
    db.run(
      itemService.get(uuid).map(ItemMapper.toDescrDto(_))
    )
  }


  private def getSavedField(ownerUuid: String, fieldUuid: String) = {
    fieldDataService.fieldFor(ownerUuid, fieldUuid)
      .map(f => FieldMapper.toDtoRf(f))
  }

  override def getItems()(implicit request: Request[AnyContent]): Future[List[ItemDescr]] = run { implicit u =>
    db.run(itemService.all()).map(_.map(ItemMapper.toDescrDto(_)).toList)
  }

  override def deleteItemField(uuid: String, uuidField: String)(implicit request: Request[AnyContent]): Future[Unit] = run { implicit user =>
    db.run(fieldDataService.fieldData(uuid, uuidField).result.flatMap(l => fieldDataService.updateFieldValues(l, Nil)))
      .map(_ => None)
  }


  override def updateItemField(uuid: String, uuidField: String, field: Field, subReferenceUuid: Option[String])(implicit request: Request[AnyContent]): Future[Field] = run { implicit user =>
    import api._
    db.run(
      (for {
        item <- itemService.getOrCreate(uuid)
        validatedField <- fieldValidationService.assertValidField(field)
        currentData <- fieldDataService.fieldData(uuid, uuidField, subReferenceUuid).result
        newFields <- fieldDataService.updateFieldValues(currentData, FieldMapper.toDataRows(validatedField, subReferenceUuid.getOrElse(uuid)))
        upd <- itemRefresher.refresh(uuid)
        field <- getSavedField(uuid, uuidField)
      } yield (field)).transactionally
    )
  }
}
