package be.frol.chaman.api

import be.frol.chaman.Conf
import be.frol.chaman.openapi.api.ServiceFieldApi
import be.frol.chaman.openapi.model.ServiceFieldValue
import be.frol.chaman.service.{FieldService, FieldTypeService}
import be.frol.chaman.utils.DebugUtils
import be.frol.chaman.utils.OptionUtils.enrichedOption
import play.api.db.slick.DatabaseConfigProvider
import play.api.libs.json.{JsArray, JsNull, JsNumber, JsObject, JsString, JsValue}
import play.api.libs.ws.WSClient
import play.api.mvc.{AbstractController, AnyContent, ControllerComponents, Request}
import play.api.{Configuration, Logging}

import javax.inject.Inject
import scala.concurrent.{ExecutionContext, Future}

class ServiceFieldApiImpl @Inject()(
                                     val cc: ControllerComponents,
                                     val dbConfigProvider: DatabaseConfigProvider,
                                     val fieldService: FieldService,
                                     val fieldTypeService: FieldTypeService,
                                     ws: WSClient,
                                   )(
                                     implicit executionContext: ExecutionContext,
                                     val configration: Configuration,
                                   ) extends AbstractController(cc) with ServiceFieldApi with ParentController with DbContext with Logging {

  override def getAllValues(uuid: String)(implicit request: Request[AnyContent]): Future[List[ServiceFieldValue]] = run {
    u =>
      def extract(v:JsValue) : String= v match {
        case JsString(s)=> s
        case JsNumber(n) => n.toString()
        case o : AnyRef => o.toString
      }
      db.run(this.fieldService.getField(uuid)
        .map(v => this.fieldTypeService.get(v.fieldType.reference)))
        .flatMap { v =>
          val serviceField = v.flatMap(f => Conf.serviceFields.find{v =>
            v.reference == f.reference
          }).getOrThrowM("service field not found " + uuid)
          ws.url(serviceField.url).get().map(r => r.json match {
            case JsArray(value) => value.map {
              case JsObject(i) => Some(ServiceFieldValue(i.get(serviceField.identifier).map(extract(_)), i.get(serviceField.value).map(extract(_))))
              case _ => None
            }.flatten.toList
            case _ => throw new RuntimeException("Service did not returned an array " + serviceField.identifier)
          })
        }
  }
}
