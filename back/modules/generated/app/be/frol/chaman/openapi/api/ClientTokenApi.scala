package be.frol.chaman.openapi.api

import play.api.libs.json._
import play.api.mvc._
import scala.concurrent.Future
import be.frol.chaman.openapi.model.ClientToken

@javax.annotation.Generated(value = Array("org.openapitools.codegen.languages.ScalaPlayFrameworkServerCodegen"))
trait ClientTokenApi {
  /**
    * create client token
    */
  def createClientTokens(clientToken: Option[ClientToken])(implicit request:Request[AnyContent]): Future[ClientToken]


  /**
    * get client tokens
    */
  def getClientTokens()(implicit request:Request[AnyContent]): Future[List[ClientToken]]


  /**
    * remove tokens
    */
  def removeClientTokens(uuid: String)(implicit request:Request[AnyContent]): Future[Unit]

}
