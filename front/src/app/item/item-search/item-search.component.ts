import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ItemDescr, ItemService} from "../../../generated/api";
import {Subject} from "rxjs";
import {MatTableDataSource} from "@angular/material/table";
import {LiveScanService} from "../../scan/live-scan.service";

@Component({
  selector: 'app-item-search',
  templateUrl: './item-search.component.html',
  styleUrls: ['./item-search.component.css']
})
export class ItemSearchComponent implements OnInit {

  @Output() selection = new EventEmitter<ItemDescr>()

  datasource = new MatTableDataSource<ItemDescr>()
  displayedColumns = ['thumbnail', 'title', 'description', 'tags']
  scanListener: Subject<ItemDescr>;
  @Input() navigateToItem: boolean = false;

  constructor(
    private itemService: ItemService,
    private liveScanService: LiveScanService,
  ) {
    this.itemService.getItems().toPromise().then(i => {
      this.datasource.data = i
    })
  }


  ngOnInit(): void {
    if(!this.navigateToItem){
      this.scanListener = this.liveScanService.listen();
      this.scanListener.subscribe(i => {
        this.select(i)
      })
    }
  }

  select(itemDesc: ItemDescr): void {
    if (!this.navigateToItem) {
      this.selection.next(itemDesc);
    }
  }

  ngOnDestroy(): void {
    this.scanListener?.complete();
  }
}
