import {Observable} from "rxjs";

export interface Autocomplete<T> {
  getSuggestions(v:string): Observable<T[]>
  get(v:string): Observable<T>
  stringRepr(t:T) : string
  add(v:string): Observable<T>
  canAdd():boolean
}
