package be.frol.chaman.api

import be.frol.chaman.openapi.api.ImportApi
import be.frol.chaman.openapi.model.Item
import be.frol.chaman.service.ItemService
import play.api.db.slick.DatabaseConfigProvider
import play.api.mvc.{AnyContent, ControllerComponents, Request}

import javax.inject.Inject
import scala.concurrent.Future

class ImportApiImpl @Inject()(
                               val cc: ControllerComponents,
                               val itemService: ItemService,
                               val dbConfigProvider: DatabaseConfigProvider,
                             ) extends ImportApi with ParentController with DbContext {

  override def importItem(itemPath: String)(implicit request: Request[AnyContent]): Future[Item] = {
    ???
  }
}
