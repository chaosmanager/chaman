package be.frol.chaman.openapi.model

import play.api.libs.json._
import java.time.OffsetDateTime

/**
  * Represents the Swagger definition for UserToken.
  */
@javax.annotation.Generated(value = Array("org.openapitools.codegen.languages.ScalaPlayFrameworkServerCodegen"))
case class UserToken(
  id: Option[String],
  date: Option[OffsetDateTime],
  value: Option[String]
)

object UserToken {
  implicit lazy val userTokenJsonFormat: Format[UserToken] = Json.format[UserToken]
}

