package be.frol.chaman.model

import be.frol.chaman.core.data.holder.Holder
import be.frol.chaman.core.field.{BasicFieldType, BasicFieldTypes, FieldType, FieldTypes, FieldWithConf}
import be.frol.chaman.tables.Tables.{DataRow, FieldRow}
import be.frol.chaman.utils.OptionUtils._
import play.api.Logging
import play.api.libs.json.{JsObject, Json}
import play.api.mvc.BodyParsers.utils

case class RichField[T <: Holder](field: FieldWithConf[T], fieldData: Iterable[DataRow]) extends Logging{
  def fieldUuid = field.uuid

  lazy val fieldType: FieldType[T] = field.fieldType

  def withData(fieldData: Map[String,Iterable[DataRow]]) = copy(fieldData = fieldData.get(fieldUuid).toList.flatten)

  def withData(fieldData: Iterable[DataRow]) = copy(fieldData = fieldData)

  def stringRepr = values.map(_.stringRepr).mkString(",")

  def values : Iterable[T] = fieldData.flatMap(_.value).map(v => fieldType.value(Json.parse(v)))

  def equivalent(other: RichField[T]) = {
    def removeNonImportant(f: FieldWithConf[T]) = (f.label, f.reference, f.config, f.fieldType.inputType, f.defaultField)
    removeNonImportant(field) == removeNonImportant(other.field)
  }

  def isEmpty:Boolean = fieldData.isEmpty
}

