--

-- !Ups

CREATE TABLE `client_access_token` (
                           `id` bigint NOT NULL AUTO_INCREMENT,
                           `uuid` varchar(36) NOT NULL,
                           `access_point` varchar(255) NOT NULL,
                           `token` varchar(36) NOT NULL,
                           `author` varchar(36) NOT NULL,
                           `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                           PRIMARY KEY (`id`)
);
CREATE TABLE client_access_token_removed (
                                           fk_client_access_token_id bigint(20) NOT NULL REFERENCES client_access_token(id),
                                           author varchar(36) NOT NULL,
                                           `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                           PRIMARY KEY (fk_client_access_token_id)
);

