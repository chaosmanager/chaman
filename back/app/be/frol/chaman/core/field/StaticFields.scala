package be.frol.chaman.core.field

import be.frol.chaman.core.data.holder.Holder
import be.frol.chaman.utils.TraversableUtils._

object StaticFields {

  def allStaticFields : List[FieldWithConf[_ <: Holder]]= DefaultFields.fields ::: ConfigFieldTypes.all
  def allStaticFieldsMap : Map[String, FieldWithConf[_ <: Holder]]= allStaticFields.toMapBy(_.uuid)

}
