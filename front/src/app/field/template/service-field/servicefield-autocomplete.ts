import {Autocomplete} from "../multiple-parent/autocomplete";
import {ServiceFieldService, ServiceFieldValue, TagService} from "../../../../generated/api";
import {Observable} from "rxjs";
import {map} from "rxjs/operators";
import { of } from 'rxjs/internal/observable/of';

export class ServicefieldAutocomplete implements Autocomplete<ServiceFieldValue>{
  allValues : Array<ServiceFieldValue>

  constructor(
    public fieldUuid: string,
    public serviceFieldService: ServiceFieldService,
  ){

  }


  getAllValues() : Observable<Array<ServiceFieldValue>>{
    if(this.allValues)
      return of(this.allValues)
    else
      return this.serviceFieldService.getAllValues(this.fieldUuid).pipe(
        map(v => {
          this.allValues = v
          return v
        })
      )
  }


  add(v: string): Observable<ServiceFieldValue> {
    return of(undefined)
  }

  getSuggestions(v: string): Observable<ServiceFieldValue[]> {
    if(!v) return this.getAllValues().pipe(map(v => v.slice()));
    const lowerCaseV = v.toLowerCase()
    return this.getAllValues().pipe(
      map( all => all.filter(i => i.value.toLowerCase().includes(lowerCaseV)))
    )
  }

  canAdd(): boolean {
    return false;
  }

  stringRepr(t: ServiceFieldValue): string {
    return t.value;
  }

  get(v: string): Observable<ServiceFieldValue> {
    return this.getAllValues().pipe(
      map(l => l.find(u => u.value === v))
    )
  }
}
