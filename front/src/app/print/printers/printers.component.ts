import {Component, OnInit} from '@angular/core';
import {MatTableDataSource} from "@angular/material/table";
import {Printer, PrintService} from "../../../generated/api";
import {RxjsHelperService} from "../../rxjs-helper.service";
import {NgbModal, NgbModalConfig} from "@ng-bootstrap/ng-bootstrap";
import {PrintQueueComponent} from "../print-queue/print-queue.component";

export class EditablePrinter {
  constructor(
    public printer:Printer,
    public edit:boolean = false,
    public editIdentifier: string = '',
  ) {
  }
  get id(){
    return this.printer.id;
  }
  get identifier(){
    return this.printer.identifier;
  }
  get lastSeen(){
    return this.printer.lastSeen;
  }

}

@Component({
  selector: 'app-printers',
  templateUrl: './printers.component.html',
  styleUrls: ['./printers.component.css']
})
export class PrintersComponent implements OnInit {

  displayedColumns = ['id', 'identifier', 'actions'];
  datasource = new MatTableDataSource<EditablePrinter>()

  constructor(
    private printService: PrintService,
    private r: RxjsHelperService,
    private modalService: NgbModal,
    private modalConfig: NgbModalConfig,
  ) {
    this.modalConfig.size = 'xl'
    this.r.wrap(this.printService.getPrinters())
      .withErrorMessage("Couldn't load printer list")
      .then(v => {
        this.datasource.data = v.map(p => new EditablePrinter(p))
      })
  }


  ngOnInit(): void {
  }

  add() {
    this.r.wrap(this.printService.addPrinter())
      .withErrorMessage("Couldn't add a printer")
      .then(v => {
        this.datasource.data.push(new EditablePrinter(v))
        this.datasource.data = this.datasource.data
      })
  }

  remove(u: EditablePrinter) {
    if (confirm("Are you sure you want to delete the printer?")) {
      this.printService.deletePrinter(u.id).toPromise().then(r => {
        let index = this.datasource.data.indexOf(u)
        this.datasource.data.splice(index, 1)
        this.datasource.data = this.datasource.data;
      })
    }
  }

  viewQueue(printer: EditablePrinter) {
    let ngbModalRef = this.modalService.open(PrintQueueComponent);
    (ngbModalRef.componentInstance as PrintQueueComponent).printerId = printer.id;
  }

  edit(p: EditablePrinter) {
    p.editIdentifier = p.identifier;
    p.edit = true;
  }

  save(p: EditablePrinter) {
    this.r.wrap(this.printService.updatePrinter(p.id, p.editIdentifier))
      .withErrorMessage("Couldn't save change")
      .withSuccessMessage("Pinter updated")
      .then(u => {
        let index = this.datasource.data.findIndex(v => v.id === u.id)
        this.datasource.data[index] = new EditablePrinter(u)
        this.datasource.data = this.datasource.data;
      })

  }
}
