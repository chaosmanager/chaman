package be.frol.chaman.core.field

import be.frol.chaman.core.data.holder.Holder
import be.frol.chaman.service.FieldTypeService
import be.frol.chaman.tables.Tables.FieldRow
import be.frol.chaman.utils.OptionUtils.enrichedOption
import play.api.libs.json.{JsObject, JsValue, Json}

trait FieldWithConf[T <: Holder] {
  def parseAndFormat(input: JsValue): JsValue = fieldType.parseAndFormat(input, config)

  def uuid: String

  def reference :String

  def label : String

  def fieldType: FieldType[T]

  def config: JsObject

  def isUserField : Boolean

  def defaultField : Boolean
}
object FieldWithConf {

  def toFieldWithConf[T <: Holder](fr:FieldRow)(implicit dataTypeService: FieldTypeService) = new FieldWithConf[T] {
    override def uuid: String = fr.uuid

    override def fieldType: FieldType[T] = dataTypeService.get(fr.datatype).getOrThrowM("Field type " + fr.datatype + " not known").asInstanceOf[FieldType[T]]

    override def config: JsObject = fr.config.map(Json.parse(_)).map{
      case s:JsObject => s
      case _ => JsObject(Nil)
    }.getOrElse(JsObject(Nil))

    override def reference: String = fr.reference

    override def label: String = fr.label

    override def isUserField: Boolean = true

    override def defaultField: Boolean = fr.defaultfield
  }
}
