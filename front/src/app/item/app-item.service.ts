import { Injectable } from '@angular/core';
import {ItemDescr, ItemService} from "../../generated/api";
import {Router} from "@angular/router";
import {RxjsHelperService} from "../rxjs-helper.service";

@Injectable({
  providedIn: 'root'
})
export class AppItemService {

  constructor(
    private routerService:Router,
    private r : RxjsHelperService,
    private itemService: ItemService,
  ) { }

  public newItem() {
    this.routerService.navigate(["/item/new"])
    this.r.wrap(this.itemService.createItem())
      .withErrorMessage("Error while creating a new item")
      .then(i => this.goTo(i))
  }

  public goTo(item: ItemDescr) {
    this.redirect(item);
  }

  private redirect(item: ItemDescr) {
    this.routerService.navigate(["/item/", item.uuid])
  }
}
