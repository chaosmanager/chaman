package be.frol.chaman.api

import akka.actor.ActorSystem
import akka.stream.Materializer
import be.frol.chaman.Conf
import be.frol.chaman.openapi.api.EventApi
import be.frol.chaman.openapi.model.Event
import be.frol.chaman.service.WebsocketService
import be.frol.chaman.utils.OptionUtils.enrichedOption
import play.api.{Configuration, Logging}
import play.api.db.slick.DatabaseConfigProvider
import play.api.libs.json.Json
import play.api.mvc.{AbstractController, AnyContent, ControllerComponents, Request}
import redis.RedisClient

import javax.inject.Inject
import scala.concurrent.{ExecutionContext, Future}

class EventApiImpl @Inject()(
                              val cc: ControllerComponents,
                              val dbConfigProvider: DatabaseConfigProvider,
                              val websocketService: WebsocketService,
                            )(implicit executionContext: ExecutionContext)
  extends AbstractController(cc) with EventApi with ParentController with DbContext with Logging{


  override def addEvent(event: Option[Event])(implicit request: Request[AnyContent]): Future[Unit] = run { implicit user =>
    websocketService.addUserEvent(event.getOrThrowM("event is mandatory"))
      .map(v => Ok(v + ""))
  }

}
