import {Component, OnDestroy, OnInit} from '@angular/core';
import {ItemDescr} from "../../../generated/api";
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {LiveScanService} from "../../scan/live-scan.service";
import {Subject} from "rxjs";

@Component({
  selector: 'app-item-selector',
  templateUrl: './item-selector-modal.component.html',
  styleUrls: ['./item-selector-modal.component.css']
})
export class ItemSelectorModalComponent implements OnInit {

  constructor(
    public modal: NgbActiveModal,
  ) { }


  ngOnInit(): void {
  }

  select(itemDesc:ItemDescr) : void {
    this.modal.close(itemDesc)
  }


}
