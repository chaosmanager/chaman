export * from './annex.service';
import { AnnexService } from './annex.service';
export * from './clientToken.service';
import { ClientTokenService } from './clientToken.service';
export * from './copyItem.service';
import { CopyItemService } from './copyItem.service';
export * from './datatypes.service';
import { DatatypesService } from './datatypes.service';
export * from './event.service';
import { EventService } from './event.service';
export * from './field.service';
import { FieldService } from './field.service';
export * from './file.service';
import { FileService } from './file.service';
export * from './gallery.service';
import { GalleryService } from './gallery.service';
export * from './import.service';
import { ImportService } from './import.service';
export * from './item.service';
import { ItemService } from './item.service';
export * from './link.service';
import { LinkService } from './link.service';
export * from './print.service';
import { PrintService } from './print.service';
export * from './scanner.service';
import { ScannerService } from './scanner.service';
export * from './serviceField.service';
import { ServiceFieldService } from './serviceField.service';
export * from './stickerTemplate.service';
import { StickerTemplateService } from './stickerTemplate.service';
export * from './tag.service';
import { TagService } from './tag.service';
export * from './thumbnail.service';
import { ThumbnailService } from './thumbnail.service';
export * from './user.service';
import { UserService } from './user.service';
export * from './userToken.service';
import { UserTokenService } from './userToken.service';
export const APIS = [AnnexService, ClientTokenService, CopyItemService, DatatypesService, EventService, FieldService, FileService, GalleryService, ImportService, ItemService, LinkService, PrintService, ScannerService, ServiceFieldService, StickerTemplateService, TagService, ThumbnailService, UserService, UserTokenService];
