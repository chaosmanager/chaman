import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'truncate'
})
export class TruncatePipe implements PipeTransform {

  transform(value: unknown, maxSize:number=30, ...args: unknown[]): unknown {
    let s = value?.toString() || "";
    if(s.length > maxSize ) {
      return s.substr(0, maxSize) + '...'
    } else return value
  }

}
