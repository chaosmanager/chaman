package be.frol.chaman.api

import akka.actor.ActorSystem
import akka.stream.Materializer
import be.frol.chaman.mapper.LinkMapper
import be.frol.chaman.openapi.api.{LinkApi, UserTokenApi}
import be.frol.chaman.openapi.model.{Link, UserToken}
import be.frol.chaman.service.{ItemService, LinkService, UserTokenService}
import be.frol.chaman.tables.Tables.{LinkRemovedRow, UserAccessTokenRow}
import be.frol.chaman.utils.DateUtils
import be.frol.chaman.utils.OptionUtils.enrichedObject
import play.api.Logging
import play.api.db.slick.DatabaseConfigProvider
import play.api.mvc.{AbstractController, AnyContent, ControllerComponents, Request}

import java.util.UUID
import javax.inject.Inject
import scala.concurrent.{ExecutionContext, Future}
import DateUtils._
import be.frol.chaman.tables.Tables
class UserTokensApiImpl @Inject()(
                             val cc: ControllerComponents,
                             val dbConfigProvider: DatabaseConfigProvider,
                             val userTokenService: UserTokenService,
                           )(implicit executionContext: ExecutionContext, system: ActorSystem, mat: Materializer)
  extends AbstractController(cc) with UserTokenApi with ParentController with DbContext with Logging {
  override def createUserTokens()(implicit request: Request[AnyContent]): Future[UserToken] = run{ implicit u =>
    db.run(
      userTokenService.create
        .map(t => toDto(t))
    )
  }

  private def toDto(t: Tables.UserAccessTokenRow) = {
    UserToken(t.uuid.toOpt, t.timestamp.toOffsetDateTime.toOpt(), t.token.toOpt())
  }

  override def getUserTokens()(implicit request: Request[AnyContent]): Future[List[UserToken]] = run{ implicit u =>
    db.run(userTokenService.getTokensFor(u.uuid)).map(_.map(toDto(_)).toList )
  }

  override def removeUserTokens(uuid: String)(implicit request: Request[AnyContent]): Future[Unit] = run{ implicit u =>
    db.run(
      userTokenService.remove(uuid).map(_ => None)
    )
  }
}
