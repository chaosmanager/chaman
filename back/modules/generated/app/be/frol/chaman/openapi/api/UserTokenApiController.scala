package be.frol.chaman.openapi.api

import org.openapitools.OpenApiExceptions
import javax.inject.{Inject, Singleton}
import play.api.libs.json._
import play.api.mvc._
import scala.concurrent.{ExecutionContext, Future}
import be.frol.chaman.openapi.model.UserToken

@javax.annotation.Generated(value = Array("org.openapitools.codegen.languages.ScalaPlayFrameworkServerCodegen"))
@Singleton
class UserTokenApiController @Inject()(cc: ControllerComponents, api: UserTokenApi)(implicit executionContext: ExecutionContext) extends AbstractController(cc) {
  /**
    * PUT /api/user-token/
    */
  def createUserTokens(): Action[AnyContent] = Action.async { request =>
    def executeApi(): Future[UserToken] = {
      api.createUserTokens()(request)
    }

    executeApi().map { result =>
      val json = Json.toJson(result)
      Ok(json)
    }
  }

  /**
    * GET /api/user-token/
    */
  def getUserTokens(): Action[AnyContent] = Action.async { request =>
    def executeApi(): Future[List[UserToken]] = {
      api.getUserTokens()(request)
    }

    executeApi().map { result =>
      val json = Json.toJson(result)
      Ok(json)
    }
  }

  /**
    * DELETE /api/user-token/:uuid
    */
  def removeUserTokens(uuid: String): Action[AnyContent] = Action.async { request =>
    def executeApi(): Future[Unit] = {
      api.removeUserTokens(uuid)(request)
    }

    executeApi().map { _ =>
      Ok
    }
  }

  private def splitCollectionParam(paramValues: String, collectionFormat: String): List[String] = {
    val splitBy =
      collectionFormat match {
        case "csv" => ",+"
        case "tsv" => "\t+"
        case "ssv" => " +"
        case "pipes" => "|+"
      }

    paramValues.split(splitBy).toList
  }
}
