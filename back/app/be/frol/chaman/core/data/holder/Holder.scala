package be.frol.chaman.core.data.holder

import be.frol.chaman.utils.TryUtils.tryEnriched

import scala.util.Try

trait Holder {
  def stringRepr: String
}
case class StringHolder(strValue: Option[String]) extends Holder {
  override def stringRepr: String = strValue.getOrElse("")
}

case class NumberHolder(strValue: Option[String], processedValue: Option[BigDecimal]) extends Holder {
  val numericValue = Try {
    strValue.map(NumberHolder.parse(_))
  }.mapError(new RuntimeException("Number not readable : " + strValue.getOrElse(""))).get


  override def stringRepr: String = strValue.getOrElse("")
}

object NumberHolder {
  val metricIndex = Map('m' -> -3, 'µ' -> -6, 'n' -> -9, 'p' -> -12, 'f' -> -15, 'a' -> -18, 'z' -> -21,
    'k' -> 3, 'M' -> 6, 'G' -> 9, 'T' -> 12, 'P' -> 15, 'E' -> 18, 'Z' -> 21).mapValues(v => BigDecimal(10).pow(v))
  val metricByShift = metricIndex.map(v => v._2 -> v._1).toMap
  val negativeSortedIndex = metricIndex.values.filter(_<1).toList.sorted.reverse
  val positiveSortedIndex = metricIndex.values.filter(_>1).toList.sorted


  def parse(s : String): BigDecimal = {
    val cleaned = s.replace(" ", "")
    if(metricIndex.keySet.contains(cleaned.last)) BigDecimal(cleaned.dropRight(1)) * metricIndex(cleaned.last)
    else BigDecimal(s)
  }

  def toMetric(v : BigDecimal) = {
    if(v >= 1 && v < 1000)  v.toString()
    else {
      val shift = {
        if(v < 1) negativeSortedIndex.find(i => v / i >= 1).getOrElse(negativeSortedIndex.last)
        else positiveSortedIndex.find(i => v / i < 1000).getOrElse(positiveSortedIndex.last)
      }
      val decimal: BigDecimal = v / shift
      decimal.bigDecimal.toPlainString + metricByShift(shift).toString
    }
  }

}

case class BooleanHolder(value: Option[Boolean]) extends Holder {
  override def stringRepr: String = value.map(_.toString).getOrElse("")
}
