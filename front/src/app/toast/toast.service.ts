import {Injectable, TemplateRef} from '@angular/core';
import {ItemDescr} from "../../generated/api";

@Injectable({providedIn: 'root'})
export class ToastService {
  toasts: any[] = [];

  show(content: string | ItemDescr | TemplateRef<any>, options: any = {}) {
    this.toasts.push({content, ...options});
  }

  showError(text: string, error: any) {
    this.show(text, {classname: 'bg-danger text-light', delay: 10000})
  }

  showSuccess(text: string) {
    this.show(text, {classname: 'bg-success text-light', delay: 10000})
  }

  remove(toast) {
    this.toasts = this.toasts.filter(t => t !== toast);
  }

  showItem(content: ItemDescr) {
    this.toasts.push({content:content, delay: 10000})
  }
}
