package be.frol.chaman.api

import be.frol.chaman.openapi.api.ClientTokenApi
import be.frol.chaman.openapi.model.ClientToken
import be.frol.chaman.service.ClientTokenService
import be.frol.chaman.tables.Tables.ClientAccessTokenRow
import be.frol.chaman.utils.DateUtils
import be.frol.chaman.utils.DateUtils._
import be.frol.chaman.utils.OptionUtils.{enrichedObject, enrichedOption}
import play.api.db.slick.DatabaseConfigProvider
import play.api.mvc.{AnyContent, ControllerComponents, Request}

import java.util.UUID
import javax.inject.Inject
import scala.concurrent.{ExecutionContext, Future}

class ClientTokenApiImpl @Inject()(
                                    val cc: ControllerComponents,
                                    val clientTokenService: ClientTokenService,
                                    val dbConfigProvider: DatabaseConfigProvider,
                                  )(
                                    implicit executionContext: ExecutionContext,
                                  ) extends ClientTokenApi with ParentController with DbContext {
  override def createClientTokens(clientToken: Option[ClientToken])(implicit request: Request[AnyContent]): Future[ClientToken] = run { implicit u =>
    db.run {
      val token = clientToken.getOrThrowM("client token required")
      clientTokenService.add(
        new ClientAccessTokenRow(
          0L,
          UUID.randomUUID().toString,
          token.accessPoint.getOrThrowM("Access point required"),
          token.value.getOrThrowM("Access point required"),
          u.uuid,
          DateUtils.ts,
        )
      ).map(toDto(_))
    }
  }

  def toDto(token: ClientAccessTokenRow) = {
    new ClientToken(
      token.uuid.toOpt,
      token.timestamp.toOffsetDateTime.toOpt(),
      token.token.toOpt(),
      token.accessPoint.toOpt(),
    )
  }

  override def getClientTokens()(implicit request: Request[AnyContent]): Future[List[ClientToken]] = run { implicit u =>
    db.run(
      clientTokenService.getTokensFor(u.uuid).map(_.map(toDto(_)).toList)
    )
  }

  override def removeClientTokens(uuid: String)(implicit request: Request[AnyContent]): Future[Unit] = run { implicit u =>
    db.run(clientTokenService.remove(uuid)).map(_ => None)
  }
}

