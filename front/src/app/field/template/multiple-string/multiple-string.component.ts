import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {MatChipInputEvent} from "@angular/material/chips";
import {FieldValueImpl} from "../../../model/FieldValueImpl";
import {FieldValue} from "../../../../generated/api";
import {FieldParent} from "../../core/fieldParent";
import {MultipleParentComponent} from "../multiple-parent/multiple-parent.component";

@Component({
  selector: 'app-multiple-string',
  templateUrl: './multiple-string.component.html',
  styleUrls: ['./multiple-string.component.css']
})
export class MultipleStringComponent extends FieldParent implements OnInit {

  constructor() {
    super();
  }

  @ViewChild('multipleParent') multipleParent: MultipleParentComponent<string>;
  beforeSaveHook() : Promise<void> {
    return this.multipleParent.beforeSaveHook()
  }
  ngOnInit(): void {
  }


}
