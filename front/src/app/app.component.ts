import {Component, ElementRef, HostListener} from '@angular/core';
import {WebsocketService} from "./websocket.service";
import {AppItemService} from "./item/app-item.service";
import {LiveScanService} from "./scan/live-scan.service";
import {MobileDetectorService} from "./mobile-detector.service";
import {ContextService} from "./context.service";
import {ItemDescr} from "../generated/api";
import {NgbModal, NgbModalConfig} from "@ng-bootstrap/ng-bootstrap";
import {MoveModalComponent} from "./item/move-modal/move-modal.component";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'chaman';
  navCollapsed = true;
  currentItem: ItemDescr;

  keypressHistory = "";

  constructor(
    private websocketService: WebsocketService,
    private appItemService: AppItemService,
    public liveScanService: LiveScanService,
    public mobileService: MobileDetectorService,
    public context: ContextService,
    private modalService: NgbModal,
    private modalConfig: NgbModalConfig,
    private elemRef: ElementRef,
  ) {
    this.modalConfig.size = 'xl'
    let protocol = (window.location.protocol.startsWith('https')) ? "wss" : "ws"
    let url = protocol + "://" + window.location.host + "/api/event/user";
    this.launchWs(url);
    this.context.itemContext.subscribe(item =>
      this.currentItem = item
    )
    this.context.initUser()
  }

  private launchWs(url: string) {
    console.log('launch ws')
    const _this = this
    try {
      this.websocketService.connect(url).subscribe(v => {
          let payload = JSON.parse(v.data);
          if (payload.eventType === "Scan" && payload.origin !== this.context.websocketClientId) {
            this.liveScanService.receivedScan(payload.itemId)
          }
          return console.log("received event", payload);
        }
      ).add(function () {
        console.log("Websocket closed, reconnecting")
        _this.launchWs(url)
      })
    } catch (e) {
      console.error("couldn't connect to the websocket at url " + url)
    }
  }

  newItem() {
    this.appItemService.newItem()
  }

  moveCurrent() {
    let ngbModalRef = this.modalService.open(MoveModalComponent);
    (ngbModalRef.componentInstance as MoveModalComponent).itemToMove = this.currentItem;

  }

  itemUrlRegex = new RegExp("http.*\/item\/([a-f0-9]{8}.[a-f0-9]{4}.[a-f0-9]{4}.[a-f0-9]{4}.[a-f0-9]{12})$")

  @HostListener('document:keydown', ['$event'])
  handleKeyboardEvent(event: KeyboardEvent) {
    let target = event.target as HTMLElement;
    if (["INPUT","TEXTAREA"].indexOf(target.tagName.toUpperCase()) == -1) {
      if (event.key.length === 1 || event.key === 'Unidentified') {

        this.keypressHistory += event.key;
        let value = this.keypressHistory;
        let regExpExecArray = this.itemUrlRegex.exec(value);
        if (regExpExecArray) {
          console.log("keypressed " + this.keypressHistory)
          let scanned = regExpExecArray[1];
          let cleaned = scanned.replace(/[^a-f0-9]/gi, "-"); // clean to manage bt scanner that messes special chars
          console.log("item scanned : " + cleaned)
          this.liveScanService.receivedScan(cleaned)
          this.keypressHistory = "";
        }
      }
      event.stopPropagation();
      return false;
    }
    return true;
  }

}
