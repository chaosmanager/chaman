package be.frol.chaman.service

import be.frol.chaman.api.DbContext
import be.frol.chaman.model.UserInfo
import be.frol.chaman.tables.Tables
import be.frol.chaman.utils.DateUtils
import play.api.db.slick.DatabaseConfigProvider
import slick.dbio.{DBIOAction, NoStream}

import java.util.concurrent.TimeUnit
import javax.inject.Inject

class ScanService @Inject()(
                             val dbConfigProvider: DatabaseConfigProvider,
                             val itemService: ItemService,
                           ) extends DbContext {


  import api._

  def add(p: Tables.ScanHistoryRow) : DBIO[Tables.ScanHistoryRow]= {
    ((Tables.ScanHistory returning Tables.ScanHistory.map(_.id)
      into ((v, id) => v.copy(id = id))) += p)
  }

  def add(uuid:String)(implicit user:UserInfo) : DBIO[Tables.ScanHistoryRow] = {
    add(Tables.ScanHistoryRow(0L, uuid, user.uuid, DateUtils.ts))
  }

  def lastWithItems(implicit user:UserInfo) = {
    Tables.ScanHistory
      .filter(_.author === user.uuid)
      .sortBy(_.id.desc)
      .take(20)
      .join(itemService.lastVersion).on(_.itemUuid === _.uuid)
      .result
  }

}
