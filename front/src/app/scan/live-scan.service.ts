import {Injectable} from '@angular/core';
import {ItemDescr, Scan, ScannerService} from "../../generated/api";
import {ToastService} from "../toast/toast.service";
import {DescCardComponent} from "../item/desc-card/desc-card.component";
import {BehaviorSubject, Subject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class LiveScanService {

  constructor(
    private scanService: ScannerService,
    private toastService: ToastService,
  ) {
    this.refresh();
  }

  historyFull: Array<Scan>
  subjectListener: Subject<ItemDescr>

  refresh() {
    this.scanService.scanHistory().toPromise().then(l => this.historyFull = l)
  }

  receivedScan(itemId: string) {
    if (!this.subjectListener || this.subjectListener.isStopped) {
      console.log("Sending itemid to subject listener "+ itemId)
      this.toastService.showItem({uuid: itemId})
    } else {
      console.log("Sending itemid to toast "+ itemId)
      this.subjectListener.next({uuid: itemId})
    }
  }

  listen() {
    if (this.subjectListener) {
      this.subjectListener.complete()
    }
    return this.subjectListener = new Subject<ItemDescr>()
  }
}
