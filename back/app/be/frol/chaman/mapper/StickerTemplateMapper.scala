package be.frol.chaman.mapper

import be.frol.chaman.openapi.model.StickerTemplate
import be.frol.chaman.openapi.model.StickerTemplateField
import be.frol.chaman.core.field.DefaultFields
import be.frol.chaman.openapi.model.Annex
import be.frol.chaman.tables.Tables
import be.frol.chaman.utils.OptionUtils.enrichedObject

object StickerTemplateMapper {

  def toDto(r: (Tables.StickerTemplateRow, Seq[Tables.StickerTemplateFieldRow])) : StickerTemplate= {
    toDto(r._1, r._2)
  }

  def toDto(st: Tables.StickerTemplateRow, fields: Seq[Tables.StickerTemplateFieldRow]): StickerTemplate = {
    StickerTemplate(st.uuid.toOpt(), st.reference.toOpt(), fields.map(f => StickerTemplateField(f.fieldUuid.toOpt(), f.weight)).toList.toOpt)
  }
}
