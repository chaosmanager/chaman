package be.frol.chaman.openapi.api

import org.openapitools.OpenApiExceptions
import javax.inject.{Inject, Singleton}
import play.api.libs.json._
import play.api.mvc._
import scala.concurrent.{ExecutionContext, Future}
import be.frol.chaman.openapi.model.Scan

@javax.annotation.Generated(value = Array("org.openapitools.codegen.languages.ScalaPlayFrameworkServerCodegen"))
@Singleton
class ScannerApiController @Inject()(cc: ControllerComponents, api: ScannerApi)(implicit executionContext: ExecutionContext) extends AbstractController(cc) {
  /**
    * PUT /api/scan/?uuid=[value]&origin=[value]
    */
  def addScan(): Action[AnyContent] = Action.async { request =>
    def executeApi(): Future[Unit] = {
      val uuid = request.getQueryString("uuid")
        .getOrElse {
          throw new OpenApiExceptions.MissingRequiredParameterException("uuid", "query string")
        }
      val origin = request.getQueryString("origin")
        
      api.addScan(uuid, origin)(request)
    }

    executeApi().map { _ =>
      Ok
    }
  }

  /**
    * GET /api/scan/
    */
  def scanHistory(): Action[AnyContent] = Action.async { request =>
    def executeApi(): Future[List[Scan]] = {
      api.scanHistory()(request)
    }

    executeApi().map { result =>
      val json = Json.toJson(result)
      Ok(json)
    }
  }

  private def splitCollectionParam(paramValues: String, collectionFormat: String): List[String] = {
    val splitBy =
      collectionFormat match {
        case "csv" => ",+"
        case "tsv" => "\t+"
        case "ssv" => " +"
        case "pipes" => "|+"
      }

    paramValues.split(splitBy).toList
  }
}
