import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {ItemDescr, ItemService} from "../../../generated/api";
import {RxjsHelperService} from "../../rxjs-helper.service";
import {AppItemService} from "../app-item.service";

@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.css']
})
export class ItemsComponent implements OnInit {

  constructor(
    private routerService: Router,
    private itemService: ItemService,
    private r : RxjsHelperService,
    private appItemService: AppItemService,
  ) { }

  ngOnInit(): void {
  }

  newItem() {
    this.appItemService.newItem();
  }

  goTo(item: ItemDescr) {
    this.appItemService.goTo(item);
  }

}
