--

-- !Ups

CREATE TABLE `sticker_template` (
                           `id` bigint NOT NULL AUTO_INCREMENT,
                           `uuid` varchar(36) NOT NULL,
                           `reference` varchar(255) NOT NULL,
                           `author` varchar(36) NOT NULL,
                           `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                           index uuid(uuid),
                           PRIMARY KEY (`id`)
);
CREATE TABLE sticker_template_removed (
                                           fk_sticker_template_id varchar(36) NOT NULL REFERENCES sticker_template(id),
                                           author varchar(36) NOT NULL,
                                           `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                           PRIMARY KEY (fk_sticker_template_id)
);


CREATE TABLE `sticker_template_field` (
                                          `id` bigint NOT NULL AUTO_INCREMENT,
                                          fk_sticker_template_id bigint(20) NOT NULL REFERENCES sticker_template(id),
                                          `field_uuid` varchar(36) NOT NULL,
                                          weight int null,
                                          show_field_name boolean not null default true,
                                          author varchar(36) NOT NULL,
                                          `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                          PRIMARY KEY (`id`)
);
