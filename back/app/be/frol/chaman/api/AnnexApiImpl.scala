package be.frol.chaman.api

import akka.util.ByteString
import be.frol.chaman.mapper.AnnexMapper
import be.frol.chaman.model.UserInfo
import be.frol.chaman.openapi.api.AnnexApi
import be.frol.chaman.openapi.model.Annex
import be.frol.chaman.service.{AnnexService, GalleryService, ItemService, ThumbnailService}
import be.frol.chaman.tables.Tables.AnnexRow
import be.frol.chaman.utils.{DateUtils, FileUtils, MimeUtils}
import be.frol.chaman.utils.OptionUtils.enrichedOption
import be.frol.chaman.{Conf, tables}
import play.api.db.slick.DatabaseConfigProvider
import play.api.libs.Files
import play.api.libs.ws.WSClient
import play.api.mvc.{AnyContent, ControllerComponents, Request}
import play.libs.Files.TemporaryFileCreator

import java.io.FileOutputStream
import java.util.UUID
import javax.inject.Inject
import javax.xml.bind.DatatypeConverter
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import slick.dbio.DBIO

class AnnexApiImpl @Inject()(
                              val cc: ControllerComponents,
                              val annexService: AnnexService,
                              val itemService: ItemService,
                              val dbConfigProvider: DatabaseConfigProvider,
                              val ws: WSClient,
                              val  thumbnailService: ThumbnailService,
                              val galleryService: GalleryService,
                            ) extends AnnexApi with ParentController with DbContext {
  override def uploadFile(uuid: String, file: Option[Files.TemporaryFile])(implicit request: Request[AnyContent]): Future[Annex] = run { implicit u =>
    request.body.asMultipartFormData.flatMap(_.file("file")).map { f =>
      addFile(uuid, file.get, f.filename, f.contentType.getOrThrowM("mime type missing"))
    }.getOrElse(throw new RuntimeException("missing file"))

  }

  def addFile(uuid:String, f:Files.TemporaryFile, filename:String, mime:String)(implicit u: UserInfo) = {
    val sha1Sum = FileUtils.addFileToAnnex(f)

    val addThumbnail = MimeUtils.isImage(mime) && !this.thumbnailService.hasThumbnail(uuid)
    db.run(
      for{
        item <- itemService.getOrCreate(uuid)
        annex <- annexService.add(AnnexRow(0L, UUID.randomUUID().toString, uuid, sha1Sum, filename, mime, u.uuid, DateUtils.ts))
        thumbnail <- if(addThumbnail) thumbnailService.setThumbnail(uuid, None, annex) else DBIO.successful(None)
      } yield (annex)
    ).map(AnnexMapper.toDto(_, Map()))

  }

  val unsupportedMimesForDownload = Set("application/xhtml+xml", "text/html", "application/x-httpd-php")
  override def addByUrl(uuid: String, url: String)(implicit request: Request[AnyContent]): Future[Annex] = run { implicit u =>
    ws.url(url).get().flatMap {
      case r if r.status >= 200 && r.status < 300 && !unsupportedMimesForDownload.contains(r.contentType) =>
        val file = Files.SingletonTemporaryFileCreator.create()
        val stream = new FileOutputStream(file)
        stream.write(r.bodyAsBytes.toArray)
        stream.close();
        addFile(uuid, file, r.header("Content-Disposition").flatMap(filename).getOrElse(url.split("/").last), r.contentType)
    }

  }

  def filename(v:String) : Option[String]= {
    val pattern = """.*filename="([^;]*)".*""".r
    v match {
      case pattern(v) => Some(v)
      case _ => None
    }
  }

  override def deleteAnnex(uuid: String)(implicit request: Request[AnyContent]): Future[Unit] = run { implicit user =>
    db.run(
      annexService.find(uuid).flatMap(a =>
        annexService.delete(new tables.Tables.AnnexRemovedRow(a.id, user.uuid, DateUtils.ts))
          .map(_ => None)
      )
    )
  }

  override def addAnnexFromGallery(uuid: String, galleryUuid: String)(implicit request: Request[AnyContent]): Future[Annex] = run{implicit user =>
    db.run(
    for {
        galleryImage <- galleryService.find(galleryUuid)
        annex <- annexService.add(AnnexRow(0L, UUID.randomUUID().toString, uuid, galleryImage.filesha, galleryImage.name, galleryImage.mimetype, user.uuid, DateUtils.ts))
        thumbnail <- if (MimeUtils.isImage(annex.mimetype) && !this.thumbnailService.hasThumbnail(uuid)) thumbnailService.setThumbnail(uuid, None, annex) else DBIO.successful(None)
      } yield AnnexMapper.toDto(annex, Map())
    )
  }
}
