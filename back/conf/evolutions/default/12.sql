--

-- !Ups


CREATE TABLE scan_history (
                      id bigint(20) NOT NULL AUTO_INCREMENT,
                      item_uuid varchar(36) NOT NULL,
                      author varchar(36) NOT NULL,
                      `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                      PRIMARY KEY (id)
);
