--

-- !Ups

CREATE TABLE `import` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `uuid` varchar(36) NOT NULL,
  `author` varchar(36) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `imported_from` varchar(255) NOT NULL,
  `original_author_uuid` varchar(36) NOT NULL,
  `original_author_username` varchar(36) NOT NULL,
  `original_timestamp` timestamp NOT NULL,
  PRIMARY KEY (`id`)
);

