--

-- !Ups


CREATE TABLE user_access_token (
                      id bigint(20) NOT NULL AUTO_INCREMENT,
                      uuid varchar(36) NOT NULL,
                      token varchar(20) NOT NULL,
                      user varchar(36) NOT NULL,
                      author varchar(36) NOT NULL,
                      `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                      PRIMARY KEY (id)
);

CREATE TABLE user_access_token_removed (
                                   fk_user_access_token_id bigint(20) NOT NULL REFERENCES user_access_token(id),
                                   author varchar(36) NOT NULL,
                                   `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                   PRIMARY KEY (fk_user_access_token_id)
);


