package be.frol.chaman.openapi.api

import play.api.libs.json._
import play.api.mvc._
import scala.concurrent.Future
import be.frol.chaman.openapi.model.StickerTemplate

@javax.annotation.Generated(value = Array("org.openapitools.codegen.languages.ScalaPlayFrameworkServerCodegen"))
trait StickerTemplateApi {
  /**
    * delete stricker template
    */
  def deleteStickerTemplate(uuid: String)(implicit request:Request[AnyContent]): Future[Unit]


  /**
    * get sticker template
    */
  def getStickerTemplate(uuid: String)(implicit request:Request[AnyContent]): Future[StickerTemplate]


  /**
    * update sticker template
    */
  def putStickerTemplate(uuid: String, stickerTemplate: Option[StickerTemplate])(implicit request:Request[AnyContent]): Future[StickerTemplate]

}
