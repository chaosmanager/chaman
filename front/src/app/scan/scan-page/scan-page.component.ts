import {Component, OnInit} from '@angular/core';
import {ItemService, ScannerService} from "../../../generated/api";
import {ContextService} from "../../context.service";

@Component({
  selector: 'app-scan-page',
  templateUrl: './scan-page.component.html',
  styleUrls: ['./scan-page.component.css']
})
export class ScanPageComponent implements OnInit {
  lastScan : any;
  lastScanUntil : number;

  constructor(
    private scanService: ScannerService,
    private itemService: ItemService,
    private contextService: ContextService,
  ) {
  }


  ngOnInit(): void {
  }

  get showLast() {
    return this.lastScan && this.lastScanUntil && this.lastScanUntil > Date.now()
  }

  addScan(v: string) {
    if (v.indexOf(window.location.host) !== -1) {
      var regex = new RegExp(/\/item\/([a-fA-F0-9\-]{36})/g, "g");
      var match = regex.exec(v);
      let uuid = match[1];

      if(uuid != this.lastScan?.uuid || !this.lastScanUntil || this.lastScanUntil < Date.now() ){
        this.lastScanUntil = Date.now()+2000
        this.lastScan = {uuid: uuid}
        this.scanService.addScan(uuid, this.contextService.websocketClientId).toPromise().then(v=> console.log("event added"))
        this.itemService.getItemDescr(uuid).toPromise().then(v => this.lastScan = v)

      }
    }
  }
}
