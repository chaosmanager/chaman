import {Component, Input, OnInit} from '@angular/core';
import {MatTableDataSource} from "@angular/material/table";
import {PrintJob, PrintService, UserToken, UserTokenService} from "../../../generated/api";

@Component({
  selector: 'app-print-queue',
  templateUrl: './print-queue.component.html',
  styleUrls: ['./print-queue.component.css']
})
export class PrintQueueComponent implements OnInit {

  displayedColumns = ['item', 'actions'];
  datasource = new MatTableDataSource<PrintJob>()


  constructor(
    private printService: PrintService,
  ) {
  }

  set printerId(pid: string){
    this.printService.getPrints(pid).toPromise().then(v => {
      this.datasource.data = v
    })

  }

  ngOnInit(): void {
  }


  remove(u: PrintJob) {
    this.printService.deleteJob(u.id).toPromise().then(r => {
      let index = this.datasource.data.indexOf(u)
      this.datasource.data.splice(index, 1)
      this.datasource.data = this.datasource.data;
    })
  }

}
