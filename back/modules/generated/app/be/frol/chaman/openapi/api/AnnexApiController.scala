package be.frol.chaman.openapi.api

import org.openapitools.OpenApiExceptions
import javax.inject.{Inject, Singleton}
import play.api.libs.json._
import play.api.mvc._
import scala.concurrent.{ExecutionContext, Future}
import be.frol.chaman.openapi.model.Annex
import play.api.libs.Files.TemporaryFile

@javax.annotation.Generated(value = Array("org.openapitools.codegen.languages.ScalaPlayFrameworkServerCodegen"))
@Singleton
class AnnexApiController @Inject()(cc: ControllerComponents, api: AnnexApi)(implicit executionContext: ExecutionContext) extends AbstractController(cc) {
  /**
    * POST /api/item/:uuid/annexFromGallery/:galleryUuid
    */
  def addAnnexFromGallery(uuid: String, galleryUuid: String): Action[AnyContent] = Action.async { request =>
    def executeApi(): Future[Annex] = {
      api.addAnnexFromGallery(uuid, galleryUuid)(request)
    }

    executeApi().map { result =>
      val json = Json.toJson(result)
      Ok(json)
    }
  }

  /**
    * PUT /api/item/:uuid/annexbyurl?url=[value]
    */
  def addByUrl(uuid: String): Action[AnyContent] = Action.async { request =>
    def executeApi(): Future[Annex] = {
      val url = request.getQueryString("url")
        .getOrElse {
          throw new OpenApiExceptions.MissingRequiredParameterException("url", "query string")
        }
      api.addByUrl(uuid, url)(request)
    }

    executeApi().map { result =>
      val json = Json.toJson(result)
      Ok(json)
    }
  }

  /**
    * DELETE /api/annex/:uuid
    */
  def deleteAnnex(uuid: String): Action[AnyContent] = Action.async { request =>
    def executeApi(): Future[Unit] = {
      api.deleteAnnex(uuid)(request)
    }

    executeApi().map { _ =>
      Ok
    }
  }

  /**
    * POST /api/item/:uuid/annex
    */
  def uploadFile(uuid: String): Action[AnyContent] = Action.async { request =>
    def executeApi(): Future[Annex] = {
      val file = request.body.asMultipartFormData.flatMap(_.file("file").map(_.ref: TemporaryFile))
        
      api.uploadFile(uuid, file)(request)
    }

    executeApi().map { result =>
      val json = Json.toJson(result)
      Ok(json)
    }
  }

  private def splitCollectionParam(paramValues: String, collectionFormat: String): List[String] = {
    val splitBy =
      collectionFormat match {
        case "csv" => ",+"
        case "tsv" => "\t+"
        case "ssv" => " +"
        case "pipes" => "|+"
      }

    paramValues.split(splitBy).toList
  }
}
