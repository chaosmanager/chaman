import {Component, Input, OnInit} from '@angular/core';
import {ItemDescr} from "../../../generated/api";

@Component({
  selector: 'app-desc-card',
  templateUrl: './desc-card.component.html',
  styleUrls: ['./desc-card.component.css']
})
export class DescCardComponent implements OnInit {

  constructor() { }

  @Input() item:ItemDescr;
  @Input() linkToItem: boolean = true;

  ngOnInit(): void {
  }

}
