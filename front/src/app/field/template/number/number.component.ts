import { Component, OnInit } from '@angular/core';
import {Field, FieldValue} from "../../../../generated/api";
import {FieldParent} from "../../core/fieldParent";

@Component({
  selector: 'app-integer',
  templateUrl: './number.component.html',
  styleUrls: ['./number.component.css']
})
export class NumberComponent extends FieldParent implements OnInit {

  constructor() {
    super();
  }

  ngOnInit(): void {
  }

}
