package be.frol.chaman.core.field

import org.scalatest.{FlatSpec, FunSuite, Matchers}

class NumberHolderTest extends FlatSpec with Matchers {

  "metric formatter" should "add the correct suffix" in {
    NumberHolder.toMetric(BigDecimal(10)) should be("10")
    NumberHolder.toMetric(BigDecimal(1000)) should be("1k")
    NumberHolder.toMetric(BigDecimal(0.001)) should be("1m")
    NumberHolder.toMetric(BigDecimal(0.01)) should be("10m")
    NumberHolder.toMetric(BigDecimal(10).pow(-22)) should be("0.1z")
    NumberHolder.toMetric(BigDecimal(10).pow(24)) should be("1000Z")
  }

}
