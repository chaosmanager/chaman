package be.frol.chaman.service

import be.frol.chaman.api.DbContext
import be.frol.chaman.tables.Tables
import be.frol.chaman.tables.Tables.{StickerTemplateFieldRow, StickerTemplateRow}
import be.frol.chaman.utils.OptionUtils.enrichedOption
import be.frol.chaman.utils.SlickUtils.enrichedResultOption
import play.api.db.slick.DatabaseConfigProvider

import javax.inject.Inject
import scala.concurrent.ExecutionContext

class StickerTemplateService @Inject()(
                                        val dbConfigProvider: DatabaseConfigProvider,
                                      ) extends DbContext {


  import api._

  def current = Tables.StickerTemplate.filterNot(st => Tables.StickerTemplate.filter(_.uuid === st.uuid).filter(_.id < st.id).exists)

  def add(p: Tables.StickerTemplateFieldRow) = {
    ((Tables.StickerTemplateField returning Tables.StickerTemplateField.map(_.id)
      into ((v, id) => v.copy(id = id))) += p)
  }

  def addAll(p: Seq[Tables.StickerTemplateFieldRow])(implicit executionContext: ExecutionContext) = {
    (Tables.StickerTemplateField ++= p).map(_ => p)
  }

  def add(p: Tables.StickerTemplateRow) = {
    ((Tables.StickerTemplate returning Tables.StickerTemplate.map(_.id)
      into ((v, id) => v.copy(id = id))) += p)
  }

  def find(uuid: String)(implicit executionContext: ExecutionContext) =
    findOpt(uuid).map(_.getOrThrowM("Sticker template not found for uuid"))
  def findOpt(uuid: String)(implicit executionContext: ExecutionContext) : DBIO[Option[(StickerTemplateRow, Seq[StickerTemplateFieldRow])]] =
    for {
      t <- current.filter(_.uuid === uuid).result.headOption
      fields <- if(t.isDefined) Tables.StickerTemplateField.filter(_.fkStickerTemplateId === t.get.id).result else DBIO.successful(Nil)
    } yield t.map(_ -> fields)

  def defaultStickerTemplateUuid:DBIO[Option[String]] = current.filter(_.reference === "default").sortBy(_.id.desc).map(_.uuid).result.headOption
}
