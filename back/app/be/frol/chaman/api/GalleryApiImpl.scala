package be.frol.chaman.api

import be.frol.chaman.mapper.AnnexMapper
import be.frol.chaman.model.UserInfo
import be.frol.chaman.openapi.api.{AnnexApi, GalleryApi}
import be.frol.chaman.openapi.model.Annex
import be.frol.chaman.service.{AnnexService, GalleryService, ItemService, ThumbnailService}
import be.frol.chaman.tables.Tables.ImageGalleryRow
import be.frol.chaman.utils.OptionUtils.enrichedOption
import be.frol.chaman.utils.{DateUtils, FileUtils, MimeUtils}
import be.frol.chaman.{Conf, tables}
import ch.qos.logback.core.util.FileUtil
import play.api.db.slick.DatabaseConfigProvider
import play.api.libs.Files
import play.api.libs.ws.WSClient
import play.api.mvc.{AnyContent, ControllerComponents, Request}
import slick.dbio.DBIO

import java.io.FileOutputStream
import java.util.UUID
import javax.inject.Inject
import javax.xml.bind.DatatypeConverter
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class GalleryApiImpl @Inject()(
                              val cc: ControllerComponents,
                              val galleryService: GalleryService,
                              val dbConfigProvider: DatabaseConfigProvider,
                              val ws: WSClient,
                              val  thumbnailService: ThumbnailService,
                            ) extends GalleryApi with ParentController with DbContext {


  override def getImages(onlyMine: Option[Boolean])(implicit request: Request[AnyContent]): Future[List[Annex]] = run { implicit u =>
    db.run(
      galleryService.all(onlyMine.filterNot(v => v).map(_ => u.uuid))

    ).map(_.map(AnnexMapper.toDto(_))).map(_.toList)
  }

  override def uploadImage(file: Option[Files.TemporaryFile])(implicit request: Request[AnyContent]): Future[Annex] = run { implicit u =>
    request.body.asMultipartFormData.flatMap(_.file("file")).map { f =>
      addFile(file.get, f.filename, f.contentType.getOrThrowM("mime type missing"))
    }.getOrElse(throw new RuntimeException("missing file"))

  }

  def addFile(f:Files.TemporaryFile, filename:String, mime:String)(implicit u: UserInfo) = {
    val sha1Sum = FileUtils.addFileToAnnex(f)
    db.run(
      for{
        imageGallery <- galleryService.add(ImageGalleryRow(0L, UUID.randomUUID().toString, sha1Sum, filename, mime, u.uuid, DateUtils.ts))
      } yield (imageGallery)
    ).map(AnnexMapper.toDto(_))
  }


}
