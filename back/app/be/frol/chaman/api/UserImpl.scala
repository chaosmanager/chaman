package be.frol.chaman.api

import akka.actor.ActorSystem
import akka.stream.Materializer
import be.frol.chaman.openapi.api.{UserApi, UserTokenApi}
import be.frol.chaman.openapi.model.{User, UserToken}
import be.frol.chaman.service.{UserService, UserTokenService}
import be.frol.chaman.tables.Tables
import be.frol.chaman.utils.DateUtils
import be.frol.chaman.utils.DateUtils._
import be.frol.chaman.utils.OptionUtils.{enrichedObject, enrichedOption}
import play.api.Logging
import play.api.db.slick.DatabaseConfigProvider
import play.api.mvc.{AbstractController, AnyContent, ControllerComponents, Request}

import java.util.UUID
import javax.inject.Inject
import scala.concurrent.{ExecutionContext, Future}

class UserImpl @Inject()(
                             val cc: ControllerComponents,
                             val dbConfigProvider: DatabaseConfigProvider,
                             val userService: UserService,
                           )(implicit executionContext: ExecutionContext, system: ActorSystem, mat: Materializer)
  extends AbstractController(cc) with UserApi with ParentController with DbContext with Logging {

  override def getUser()(implicit request: Request[AnyContent]): Future[User] = run{ u =>
    db.run(
      userService.get(u.uuid)
        .map(_.getOrThrowM("Could not find user"))
        .map(u => User(u.uuid.toOpt(), u.username.toOpt()))
    )
  }
}
