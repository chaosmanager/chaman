package be.frol.chaman.api

import be.frol.chaman.error.AuthentificationError
import be.frol.chaman.model.UserInfo
import be.frol.chaman.service.{UserService, UserTokenService}
import be.frol.chaman.utils.OptionUtils.enrichedOption
import be.frol.chaman.utils.SlickUtils.enrichedResultOption
import play.api.Logging
import play.api.mvc.{Request, RequestHeader}

import scala.concurrent.{ExecutionContext, Future}

trait ParentController extends DbContext {
  import api._


  def run[T](f: UserInfo => Future[T])(implicit request: RequestHeader, executionContext: ExecutionContext) = {
    db.run(getUser(request))
      .flatMap(f(_))

  }

  def getUser(request: RequestHeader)(implicit executionContext: ExecutionContext): DBIO[UserInfo] = {
    def getForToken(token: String)(implicit executionContext: ExecutionContext) = {
      UserTokenService.activeTokens(this)
        .filter(_.token === token)
        .map(_.user)
        .result
        .headOrThrow("Invalid token")

    }

    request.session.get("user").map(DBIO.successful(_))
      .orElse(request.headers.get("USER-TOKEN").map(getForToken(_)))
      .getOrThrow(new AuthentificationError())
      .map(UserInfo(_))

  }
}


