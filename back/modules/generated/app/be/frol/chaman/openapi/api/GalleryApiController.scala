package be.frol.chaman.openapi.api

import org.openapitools.OpenApiExceptions
import javax.inject.{Inject, Singleton}
import play.api.libs.json._
import play.api.mvc._
import scala.concurrent.{ExecutionContext, Future}
import be.frol.chaman.openapi.model.Annex
import play.api.libs.Files.TemporaryFile

@javax.annotation.Generated(value = Array("org.openapitools.codegen.languages.ScalaPlayFrameworkServerCodegen"))
@Singleton
class GalleryApiController @Inject()(cc: ControllerComponents, api: GalleryApi)(implicit executionContext: ExecutionContext) extends AbstractController(cc) {
  /**
    * GET /api/gallery?onlyMine=[value]
    */
  def getImages(): Action[AnyContent] = Action.async { request =>
    def executeApi(): Future[List[Annex]] = {
      val onlyMine = request.getQueryString("onlyMine")
        .map(value => value.toBoolean)
      api.getImages(onlyMine)(request)
    }

    executeApi().map { result =>
      val json = Json.toJson(result)
      Ok(json)
    }
  }

  /**
    * POST /api/gallery
    */
  def uploadImage(): Action[AnyContent] = Action.async { request =>
    def executeApi(): Future[Annex] = {
      val file = request.body.asMultipartFormData.flatMap(_.file("file").map(_.ref: TemporaryFile))
        
      api.uploadImage(file)(request)
    }

    executeApi().map { result =>
      val json = Json.toJson(result)
      Ok(json)
    }
  }

  private def splitCollectionParam(paramValues: String, collectionFormat: String): List[String] = {
    val splitBy =
      collectionFormat match {
        case "csv" => ",+"
        case "tsv" => "\t+"
        case "ssv" => " +"
        case "pipes" => "|+"
      }

    paramValues.split(splitBy).toList
  }
}
