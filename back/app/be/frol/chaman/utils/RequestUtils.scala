package be.frol.chaman.utils

import play.api.mvc.Request

object RequestUtils {
  def isSecure(request:Request[_]) = request.secure ||
    request.headers.get("X-Forwarded-Scheme").map(_ == "https").getOrElse(false) ||
    request.headers.get("X-Forwarded-Proto").map(_ == "https").getOrElse(false)
}
