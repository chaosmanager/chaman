package be.frol.chaman.service

import be.frol.chaman.api.DbContext
import be.frol.chaman.model.{PrintJobStatus, UserInfo}
import be.frol.chaman.tables
import be.frol.chaman.tables.Tables
import be.frol.chaman.tables.Tables.PrinterRow
import be.frol.chaman.utils.{DateUtils, DebugUtils}
import be.frol.chaman.utils.SlickUtils._
import play.api.db.slick.DatabaseConfigProvider

import java.util.UUID
import javax.inject.Inject
import scala.concurrent.{ExecutionContext, Future}

class PrintService @Inject()(
                              val dbConfigProvider: DatabaseConfigProvider,
                            ) extends DbContext {


  import api._


  val lastStatus = Tables.PrintStatus.filterNot(v => Tables.PrintStatus.filter(_.printId === v.printId).filter(_.id > v.id).exists)

  val lastPrinters = Tables.Printer
    .filterNot(v => Tables.PrinterDeleted.filter(_.fkPrinterId === v.id).exists)
    .filterNot(v => Tables.Printer.filter(_.uuid === v.uuid).filter(_.id > v.id).exists)

  def add(p: Tables.PrintJobRow) = {
    ((Tables.PrintJob returning Tables.PrintJob.map(_.id)
      into ((v, id) => v.copy(id = id))) += p)
  }

  def add(p: Tables.PrinterRow) = {
    ((Tables.Printer returning Tables.Printer.map(_.id)
      into ((v, id) => v.copy(id = id))) += p)
  }

  def add(p: Tables.PrinterDeletedRow) = {
    Tables.PrinterDeleted += p
  }

  def add(p: Tables.PrintStatusRow) = {
    ((Tables.PrintStatus returning Tables.PrintStatus.map(_.id)
      into ((v, id) => v.copy(id = id))) += p)
  }

  def create(printerUuid: String,itemUuid: String)(implicit user: UserInfo, executionContext: ExecutionContext) = {
    for {
      job <- add(Tables.PrintJobRow(0L, UUID.randomUUID().toString, itemUuid, None, user.uuid, DateUtils.ts, printerUuid))
      status <- add(status(job, PrintJobStatus.ToPrint))
    } yield (job, status)
  }

  def createPrinter()(implicit user: UserInfo, executionContext: ExecutionContext) = {
    add(Tables.PrinterRow(0l, UUID.randomUUID().toString, "New printer", user.uuid, DateUtils.ts))
  }

  def updatePrinter(printerUuid:String, newName: String)(implicit user: UserInfo, executionContext: ExecutionContext) = {
    add(Tables.PrinterRow(0l, printerUuid, newName, user.uuid, DateUtils.ts))
  }

  def deletePrinter(uuid:String)(implicit user: UserInfo, executionContext: ExecutionContext) = {
    lastPrinters.filter(_.uuid === uuid).result.headOrThrow("Printer not found")
      .flatMap(v => add(Tables.PrinterDeletedRow(v.id,user.uuid, DateUtils.ts)))
  }

  private def status(v: tables.Tables.PrintJobRow, printJobStatus: PrintJobStatus.Value)(implicit user: UserInfo) = {
    Tables.PrintStatusRow(0L, v.id, printJobStatus.toString, user.uuid, DateUtils.ts)
  }

  def getToPrint(printerUuid: String)(implicit u: UserInfo) = {
    Tables.PrintJob.filter(_.printerUuid === printerUuid)
      .join(lastStatus).on(_.id === _.printId)
      .filter(_._2.status === PrintJobStatus.ToPrint.toString)
      .result
  }

  def setStatus(uuid: String, s: String)(implicit userInfo: UserInfo, executionContext: ExecutionContext) = {
    Tables.PrintJob.filter(_.uuid === uuid).result.headOrThrow("no print job " + uuid)
      .flatMap(v => add(status(v, PrintJobStatus.withName(s))).map(v -> _))
  }

  def getPrinters(): DBIO[Seq[PrinterRow]] = lastPrinters.result
}

