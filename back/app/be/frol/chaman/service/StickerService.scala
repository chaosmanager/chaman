package be.frol.chaman.service

import be.frol.chaman.api.DbContext
import be.frol.chaman.utils.TraversableUtils.traversableEnriched
import play.api.db.slick.DatabaseConfigProvider

import javax.inject.Inject
import scala.concurrent.ExecutionContext

class StickerService @Inject()(
                                val dbConfigProvider: DatabaseConfigProvider,
                                val stickerTemplateService: StickerTemplateService,
                                val fieldService: FieldService,
                                val fieldDataService: DataService,
                              ) extends DbContext {

  import api._

  def infosFor(templateUuid: String, itemUuid:String)(implicit executionContext: ExecutionContext) = {
    for {
      template <- stickerTemplateService.findOpt(templateUuid)
      templateFields = template.map(_._2).getOrElse(Nil).toMapBy(_.fieldUuid)
      data <- DBIO.sequence(templateFields.keys.map(fuuid => fieldDataService.fieldFor(itemUuid,fuuid)))
      } yield data.filterNot(_.isEmpty).toList.sortBy(f => templateFields.get(f.fieldUuid).map(_.weight.getOrElse(Int.MaxValue)))
      .map(v => v.field.label -> v.stringRepr)
  }
}
