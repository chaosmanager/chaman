package be.frol.chaman.model
import be.frol.chaman.core.data.holder.Holder
import be.frol.chaman.core.field.FieldWithConf
import be.frol.chaman.service.FieldTypeService
import be.frol.chaman.tables.Tables._

object RichModelConversions {
  implicit def fieldToFieldWithConf(field:FieldRow)(implicit dataTypeService: FieldTypeService) = FieldWithConf.toFieldWithConf(field)
  implicit def fieldToRichField(field:FieldRow)(implicit dataTypeService: FieldTypeService) = new RichField(FieldWithConf.toFieldWithConf(field),Nil)
  implicit def fieldToRichField[T <: Holder](field:FieldWithConf[T]) = new RichField[T](field,Nil)
  implicit def itemToRichItem(item:ItemRow) = new RichItem(item, Nil)

}
