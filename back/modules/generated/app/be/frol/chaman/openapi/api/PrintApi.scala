package be.frol.chaman.openapi.api

import play.api.libs.json._
import play.api.mvc._
import scala.concurrent.Future
import be.frol.chaman.openapi.model.PrintJob
import be.frol.chaman.openapi.model.Printer

@javax.annotation.Generated(value = Array("org.openapitools.codegen.languages.ScalaPlayFrameworkServerCodegen"))
trait PrintApi {
  /**
    * add a print job
    * @param uuid printer uuid
    */
  def addPrint(uuid: String, itemUuid: String)(implicit request:Request[AnyContent]): Future[PrintJob]


  /**
    * add a printer
    */
  def addPrinter()(implicit request:Request[AnyContent]): Future[Printer]


  /**
    * delete job
    */
  def deleteJob(uuid: String)(implicit request:Request[AnyContent]): Future[PrintJob]


  /**
    * delete a printer
    */
  def deletePrinter(uuid: String)(implicit request:Request[AnyContent]): Future[Unit]


  /**
    * get list of printers
    */
  def getPrinters()(implicit request:Request[AnyContent]): Future[List[Printer]]


  /**
    * get waiting print for a printer
    */
  def getPrints(uuid: String)(implicit request:Request[AnyContent]): Future[List[PrintJob]]


  /**
    * set status
    */
  def setStatus(uuid: String, status: String)(implicit request:Request[AnyContent]): Future[PrintJob]


  /**
    * update a printer
    */
  def updatePrinter(uuid: String, newIdentifier: String)(implicit request:Request[AnyContent]): Future[Printer]

}
