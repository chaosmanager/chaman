--

-- !Ups


alter table print_job add column printer_uuid varchar(36) NOT NULL default '0';

CREATE TABLE `printer` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `uuid` varchar(36) NOT NULL,
  `identifier` varchar(255) NOT NULL,
  `author` varchar(36) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
);

create table `printer_last_seen` (
     `printer_uuid` varchar(36) NOT NULL,
     `author` varchar(36) NOT NULL,
     `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
);


CREATE TABLE printer_deleted (
                      fk_printer_id bigint(20) NOT NULL REFERENCES printer(id),
                      author varchar(36) NOT NULL,
                      `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                      PRIMARY KEY (fk_printer_id)
);
