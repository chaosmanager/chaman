package be.frol.chaman.model

import be.frol.chaman.core.data.holder.Holder
import be.frol.chaman.core.field.{ConfigFieldType, ConfigurableFieldType, FieldType}
import be.frol.chaman.openapi.model.FieldConfig
import be.frol.chaman.utils.OptionUtils.enrichedObject
import play.api.libs.json.{Format, Json}

case class ServiceFieldHolder(identifier:String, value:String) extends Holder {
  override def stringRepr: String = value
}
case class ServiceFieldType(typeName:String, url:String, identifier:String, value: String) extends FieldType[ServiceFieldHolder]{

  def toConfigDto = FieldConfig(None, None, typeName.toOpt, None, None)


  override def inputType: String = "serviceField"

  override def format: Format[ServiceFieldHolder]  = ServiceFieldType.format

  override def reference: String = typeName

  override def configFields: List[ConfigFieldType[_ <: Holder]] = Nil
}

object ServiceFieldType {
  val format= Json.format[ServiceFieldHolder]
}
