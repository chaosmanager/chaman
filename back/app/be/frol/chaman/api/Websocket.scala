package be.frol.chaman.api

import akka.actor.{ActorRef, ActorSystem, Props}
import akka.stream.Materializer
import be.frol.chaman.Conf
import be.frol.chaman.actors.SubscribeActor
import be.frol.chaman.model.UserInfo
import play.api.db.slick.DatabaseConfigProvider
import play.api.libs.streams.ActorFlow
import play.api.mvc.{AbstractController, ControllerComponents, WebSocket}
import play.api.{Configuration, Logging}
import redis.RedisClient

import javax.inject.Inject
import scala.concurrent.ExecutionContext


class Websocket @Inject()(cc: ControllerComponents,
                          val dbConfigProvider: DatabaseConfigProvider,
                         )
                         (implicit executionContext: ExecutionContext, system: ActorSystem, mat: Materializer, conf: Configuration)
  extends AbstractController(cc) with ParentController with Logging {

  val redisClient = RedisClient(Conf.redisHost)

  def userEvents() = events(_.uuid)

  def printerEvents(uuid: String) = events(_ => uuid)

  private def events(f: UserInfo => String) = WebSocket.acceptOrResult[String, String] { implicit request => {
    db.run(getUser(request)).map { implicit user =>
      def props(channel: String)(out: ActorRef) = Props(classOf[SubscribeActor], redisClient,
        out, Seq(channel), Nil)

      ActorFlow.actorRef { out =>
        props(f(user))(out)
      }
    }.map(Right(_))
  }}
}
