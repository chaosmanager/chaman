package be.frol.chaman.openapi.model

import play.api.libs.json._
import java.time.OffsetDateTime

/**
  * Represents the Swagger definition for Printer.
  */
@javax.annotation.Generated(value = Array("org.openapitools.codegen.languages.ScalaPlayFrameworkServerCodegen"))
case class Printer(
  id: Option[String],
  identifier: Option[String],
  lastSeen: Option[OffsetDateTime]
)

object Printer {
  implicit lazy val printerJsonFormat: Format[Printer] = Json.format[Printer]
}

