package be.frol.chaman.openapi.api

import play.api.libs.json._
import play.api.mvc._
import scala.concurrent.Future
import be.frol.chaman.openapi.model.Item

@javax.annotation.Generated(value = Array("org.openapitools.codegen.languages.ScalaPlayFrameworkServerCodegen"))
trait ImportApi {
  /**
    * import an item from another chaman
    */
  def importItem(itemPath: String)(implicit request:Request[AnyContent]): Future[Item]

}
