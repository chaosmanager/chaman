package be.frol.chaman.utils

import org.scalatest.{FlatSpec, Matchers}

import java.io.File
import java.util.{Base64, UUID}
import javax.imageio.ImageIO

class PdfUtilsTest extends FlatSpec with Matchers {

  val uuid = UUID.randomUUID().toString

  "PdfUtils" should "render image" in {
    val image = PdfUtils.toImage(views.html.Sticker.apply(
      Base64.getEncoder().encodeToString(qrCode),
      uuid.take(8),
      "test",
      "test",
      100,
      50,
    ).toString())

  }


  val qrCode = {
    val stream = QrUtils.qrCodeOS("https://chaman.local/" + uuid)
    val r = stream.toByteArray
    stream.close()
    r
  }

}
