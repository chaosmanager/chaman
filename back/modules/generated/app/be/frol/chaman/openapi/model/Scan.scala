package be.frol.chaman.openapi.model

import play.api.libs.json._
import java.time.OffsetDateTime

/**
  * Represents the Swagger definition for Scan.
  */
@javax.annotation.Generated(value = Array("org.openapitools.codegen.languages.ScalaPlayFrameworkServerCodegen"))
case class Scan(
  date: Option[OffsetDateTime],
  item: Option[ItemDescr]
)

object Scan {
  implicit lazy val scanJsonFormat: Format[Scan] = Json.format[Scan]
}

