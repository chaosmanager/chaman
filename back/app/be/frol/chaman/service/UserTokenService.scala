package be.frol.chaman.service

import be.frol.chaman.api.DbContext
import be.frol.chaman.model.UserInfo
import be.frol.chaman.tables.Tables
import be.frol.chaman.tables.Tables.UserAccessTokenRemovedRow
import be.frol.chaman.utils.DateUtils
import be.frol.chaman.utils.OptionUtils._
import play.api.db.slick.DatabaseConfigProvider
import slick.jdbc.JdbcProfile

import java.util.UUID
import javax.inject.Inject
import scala.concurrent.ExecutionContext
import scala.util.Random

class UserTokenService @Inject()(
                                  val dbConfigProvider: DatabaseConfigProvider,
                                ) extends DbContext {


  import api._


  def create(implicit u: UserInfo) = add(Tables.UserAccessTokenRow(0L, UUID.randomUUID().toString, UserTokenService.randomToken, u.uuid, u.uuid, DateUtils.ts))

  def add(p: Tables.UserAccessTokenRow) = {
    ((Tables.UserAccessToken returning Tables.UserAccessToken.map(_.id)
      into ((v, id) => v.copy(id = id))) += p)
  }


  val activeTokens = UserTokenService.activeTokens(this)

  def getTokensFor(userUuid: String) = {
    activeTokens.filter(_.user === userUuid).result
  }

  def remove(uuid: String)(implicit user: UserInfo, executionContext: ExecutionContext) = {
    activeTokens.filter(_.uuid === uuid).result.headOption.map(_.getOrThrowM("No active token with id : " + uuid))
      .flatMap(v => Tables.UserAccessTokenRemoved += UserAccessTokenRemovedRow(v.id, user.uuid, DateUtils.ts))
  }
}

object UserTokenService {
  val alphabet = Array('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9')

  def randomToken = List.fill(20)(alphabet(Random.nextInt(alphabet.size))).mkString("")

  def activeTokens (dbC:DbContext)= {
    import dbC.api._
    Tables.UserAccessToken.filterNot(u => Tables.UserAccessTokenRemoved.filter(_.fkUserAccessTokenId === u.id).exists)
  }
}
