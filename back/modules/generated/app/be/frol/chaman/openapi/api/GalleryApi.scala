package be.frol.chaman.openapi.api

import play.api.libs.json._
import play.api.mvc._
import scala.concurrent.Future
import be.frol.chaman.openapi.model.Annex
import play.api.libs.Files.TemporaryFile

@javax.annotation.Generated(value = Array("org.openapitools.codegen.languages.ScalaPlayFrameworkServerCodegen"))
trait GalleryApi {
  /**
    * get images
    */
  def getImages(onlyMine: Option[Boolean])(implicit request:Request[AnyContent]): Future[List[Annex]]


  /**
    * upload animage to the gallery
    */
  def uploadImage(file: Option[TemporaryFile])(implicit request:Request[AnyContent]): Future[Annex]

}
