package be.frol.chaman


import be.frol.chaman.model.ServiceFieldType
import com.typesafe.config.Config
import play.api.{ConfigLoader, Configuration}

import java.io.File
import java.nio.file.FileSystems
import scala.reflect.io.Path

object Conf {

  def thumbnail(uuid: String): File =
    FileSystems.getDefault.getPath(Conf.fileStoragePath.path, "thumbnails", uuid + ".jpg").toFile

  def annex(sha1Sum: String): File =
    FileSystems.getDefault.getPath(Conf.fileStoragePath.path, "annexes", sha1Sum).toFile

  val frontRootUrl = "/"

  def qrCodePrefix(implicit conf: Configuration) = conf.get[String]("chaman.url") + "/item/"

  def openIdUrl(implicit conf: Configuration) = conf.get[String]("openID.url")

  def tokenEndpoint(implicit conf: Configuration) = conf.getOptional[String]("openID.token_endpoint")

  def client_id(implicit conf: Configuration) = conf.getOptional[String]("openID.client_id").getOrElse("chaman")

  def client_secret(implicit conf: Configuration) = conf.getOptional[String]("openID.client_secret")

  val thumbnailWidth = 200

  val fileStoragePath = Path("/data/")


  def redisHost(implicit conf: Configuration) = conf.getOptional[String]("redis.host").getOrElse("redis")

  implicit val configLoader: ConfigLoader[ServiceFieldType] = new ConfigLoader[ServiceFieldType] {
    def load(rootConfig: Config, path: String): ServiceFieldType = {
      val config = rootConfig.getConfig(path)
      ServiceFieldType(
        typeName = "toBeDefined",
        url = config.getString("url"),
        identifier = config.getString("identifier"),
        value = config.getString("value"),
      )
    }
  }

  def serviceFields(implicit conf: Configuration) = conf.getOptional[Map[String, ServiceFieldType]]("chaman.serviceFieldType")
    .map(_.toList.map{case (k, v) => v.copy(typeName = k)}).getOrElse(Nil)

}
