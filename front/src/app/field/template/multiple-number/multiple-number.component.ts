import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FieldParent} from "../../core/fieldParent";
import {MultipleParentComponent} from "../multiple-parent/multiple-parent.component";

@Component({
  selector: 'app-multiple-integer',
  templateUrl: './multiple-number.component.html',
  styleUrls: ['./multiple-number.component.css']
})
export class MultipleNumberComponent extends FieldParent implements OnInit {

  constructor() {
    super();
  }

  @ViewChild('multipleParent') multipleParent: MultipleParentComponent<number>;
  beforeSaveHook() : Promise<void>{
    return this.multipleParent.beforeSaveHook()
  }

  ngOnInit(): void {
  }



}
