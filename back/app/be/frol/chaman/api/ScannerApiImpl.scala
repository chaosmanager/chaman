package be.frol.chaman.api

import akka.actor.ActorSystem
import akka.stream.Materializer
import be.frol.chaman.Conf
import be.frol.chaman.openapi.api.{EventApi, ScannerApi, TagApi}
import be.frol.chaman.openapi.model.{Event, Scan}
import be.frol.chaman.service.{ScanService, TagService, WebsocketService}
import be.frol.chaman.tables.Tables.FieldTagRow
import be.frol.chaman.utils.DateUtils
import be.frol.chaman.utils.OptionUtils.{enrichedObject, enrichedOption}
import play.api.Logging
import play.api.db.slick.DatabaseConfigProvider
import play.api.libs.json.Json
import play.api.mvc.{AbstractController, AnyContent, ControllerComponents, Request}
import redis.RedisClient
import DateUtils._
import be.frol.chaman.mapper.ItemMapper
import be.frol.chaman.openapi.model.Event.EventType

import javax.inject.Inject
import scala.concurrent.{ExecutionContext, Future}

class ScannerApiImpl @Inject()(
                              val cc: ControllerComponents,
                              val scanService: ScanService,
                              val dbConfigProvider: DatabaseConfigProvider,
                              val websocketService: WebsocketService,
                            )(implicit executionContext: ExecutionContext, system: ActorSystem, mat: Materializer)
  extends AbstractController(cc) with ScannerApi with ParentController with DbContext with Logging{

  override def addScan(uuid: String, origin:Option[String])(implicit request: Request[AnyContent]): Future[Unit] = run(implicit user =>
      db.run(
        this.scanService.add(uuid)
      )
        .flatMap(_ => websocketService.addUserEvent(new Event(EventType.Scan.toOpt(), uuid.toOpt(), None, origin)))
        .map(_ => None)
  )

  override def scanHistory()(implicit request: Request[AnyContent]): Future[List[Scan]] = run{implicit user =>
    db.run(
      this.scanService.lastWithItems.map(_.map{case (s, i) => Scan(s.timestamp.toOffsetDateTime.toOpt, ItemMapper.toDescrDto(i).toOpt)}.toList)
    )
  }
}
