import {Component, OnInit} from '@angular/core';
import {UserToken, UserTokenService} from "../../../../generated/api";
import {MatTableDataSource} from "@angular/material/table";

@Component({
  selector: 'app-tokens-page',
  templateUrl: './tokens-page.component.html',
  styleUrls: ['./tokens-page.component.css']
})
export class TokensPageComponent implements OnInit {
  displayedColumns = ['date', 'value', 'actions'];
  datasource = new MatTableDataSource<UserToken>()

  constructor(
    private userTokenService: UserTokenService,
  ) {
    this.userTokenService.getUserTokens().toPromise().then(v => {
      this.datasource.data = v
    })
  }


  ngOnInit(): void {
  }

  newToken() {
    this.userTokenService.createUserTokens().toPromise().then(v => {
      this.datasource.data.push(v);
      this.datasource.data = this.datasource.data;
    })
  }

  remove(u: UserToken) {
    this.userTokenService.removeUserTokens(u.id).toPromise().then(r => {
      let index = this.datasource.data.indexOf(u)
      this.datasource.data.splice(index, 1)
      this.datasource.data = this.datasource.data;
    })
  }
}
