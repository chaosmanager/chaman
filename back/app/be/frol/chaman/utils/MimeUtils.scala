package be.frol.chaman.utils

object MimeUtils {

  def isImage(mime:String) = mime.toLowerCase.startsWith("image")
}
