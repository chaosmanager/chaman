package be.frol.chaman.api

import akka.actor.ActorSystem
import akka.stream.Materializer
import be.frol.chaman.mapper.StickerTemplateMapper
import be.frol.chaman.openapi.api.StickerTemplateApi
import be.frol.chaman.openapi.model.StickerTemplate
import be.frol.chaman.service.StickerTemplateService
import be.frol.chaman.tables.Tables
import be.frol.chaman.utils.{DateUtils, DebugUtils}
import be.frol.chaman.utils.OptionUtils.enrichedOption
import play.api.Logging
import play.api.db.slick.DatabaseConfigProvider
import play.api.mvc.{AbstractController, AnyContent, ControllerComponents, Request}

import javax.inject.Inject
import scala.concurrent.{ExecutionContext, Future}

class StickerTemplateApiImpl @Inject()(
                                        val cc: ControllerComponents,
                                        val dbConfigProvider: DatabaseConfigProvider,
                                        val stickerTemplateService: StickerTemplateService,
                                      )(implicit executionContext: ExecutionContext, system: ActorSystem, mat: Materializer)
  extends AbstractController(cc) with StickerTemplateApi with ParentController with DbContext with Logging {
  override def deleteStickerTemplate(uuid: String)(implicit request: Request[AnyContent]): Future[Unit] = ???

  override def getStickerTemplate(uuid: String)(implicit request: Request[AnyContent]): Future[StickerTemplate] = run { u =>
    db.run(
      stickerTemplateService.find(uuid)
        .map(StickerTemplateMapper.toDto(_))
    )
  }

  override def putStickerTemplate(uuid: String, stickerTemplate: Option[StickerTemplate])(implicit request: Request[AnyContent]): Future[StickerTemplate] = run { implicit user =>
    val ts = DateUtils.ts
    db.run(
      for {
        st <- stickerTemplateService.add(Tables.StickerTemplateRow(0L, uuid, stickerTemplate.flatMap(_.reference).getOrThrowM("reference is mandatory"), user.uuid, ts))
        stf <- stickerTemplateService.addAll(
          stickerTemplate.flatMap(_.fields).getOrElse(Nil).map(f =>
            Tables.StickerTemplateFieldRow(0L, st.id,
              f.id.getOrThrowM("Field id mandatory"),
              f.weight,
              true,
              user.uuid,
              ts
            )))
      } yield StickerTemplateMapper.toDto(st, stf)
    )
  }
}
