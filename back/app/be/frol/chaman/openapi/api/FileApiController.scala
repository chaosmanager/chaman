package be.frol.chaman.openapi.api

import be.frol.chaman.Conf
import be.frol.chaman.api.DbContext
import be.frol.chaman.service.{AnnexService, GalleryService}
import be.frol.chaman.utils.MimeUtils
import play.api.db.slick.DatabaseConfigProvider
import play.api.mvc._

import javax.inject.{Inject, Singleton}
import scala.concurrent.ExecutionContext

@Singleton
class FileApiController @Inject()(
                                   val cc: ControllerComponents,
                                   val annexService: AnnexService,
                                   val galleryService: GalleryService,
                                   val dbConfigProvider: DatabaseConfigProvider,
                                 )(implicit executionContext: ExecutionContext)
  extends AbstractController(cc) with DbContext {
  /**
   * GET /api/annex/:uuid/file
   */
  def getAnnexFile(uuid: String): Action[AnyContent] = Action.async { request =>
    db.run(
      annexService.find(uuid)
        .map(a => {
          val result = Ok(java.nio.file.Files.readAllBytes(Conf.annex(a.filesha).toPath))
            .as(a.mimetype)
          if(MimeUtils.isImage(a.mimetype)) result
          else result
            .withHeaders("Content-Disposition" -> ("attachment; filename=\"" + a.name + "\""))
        }
        )
    )
  }

  def getImageFile(uuid: String): Action[AnyContent] = Action.async { request =>
    db.run(
      galleryService.find(uuid)
        .map(a => {
          val result = Ok(java.nio.file.Files.readAllBytes(Conf.annex(a.filesha).toPath))
            .as(a.mimetype)
          if (MimeUtils.isImage(a.mimetype)) result
          else result
            .withHeaders("Content-Disposition" -> ("attachment; filename=\"" + a.name + "\""))
        }
        )
    )
  }

  def getThumbnailFile(uuid: String) = Action { request =>
    val path = Conf.thumbnail(uuid).toPath
    if (path.toFile.exists()) {
      Ok(java.nio.file.Files.readAllBytes(path)).as("image/jpg")
    } else Redirect("/assets/missingPicture.jpg")
  }

}
