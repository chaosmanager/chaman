package be.frol.chaman.openapi.model

import play.api.libs.json._

/**
  * Represents the Swagger definition for StickerTemplate.
  */
@javax.annotation.Generated(value = Array("org.openapitools.codegen.languages.ScalaPlayFrameworkServerCodegen"))
case class StickerTemplate(
  id: Option[String],
  reference: Option[String],
  fields: Option[List[StickerTemplateField]]
)

object StickerTemplate {
  implicit lazy val stickerTemplateJsonFormat: Format[StickerTemplate] = Json.format[StickerTemplate]
}

