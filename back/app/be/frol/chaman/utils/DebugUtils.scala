package be.frol.chaman.utils

import play.api.{Logger, Logging}

object DebugUtils extends Logging {
  def log(m:String, parameters: AnyRef*) = logger.error(m + " : " + parameters.map(_.toString).mkString(","))

}
