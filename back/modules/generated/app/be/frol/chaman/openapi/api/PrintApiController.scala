package be.frol.chaman.openapi.api

import org.openapitools.OpenApiExceptions
import javax.inject.{Inject, Singleton}
import play.api.libs.json._
import play.api.mvc._
import scala.concurrent.{ExecutionContext, Future}
import be.frol.chaman.openapi.model.PrintJob
import be.frol.chaman.openapi.model.Printer

@javax.annotation.Generated(value = Array("org.openapitools.codegen.languages.ScalaPlayFrameworkServerCodegen"))
@Singleton
class PrintApiController @Inject()(cc: ControllerComponents, api: PrintApi)(implicit executionContext: ExecutionContext) extends AbstractController(cc) {
  /**
    * PUT /api/printer/:uuid/print-queue/?itemUuid=[value]
    * @param uuid printer uuid
    */
  def addPrint(uuid: String): Action[AnyContent] = Action.async { request =>
    def executeApi(): Future[PrintJob] = {
      val itemUuid = request.getQueryString("itemUuid")
        .getOrElse {
          throw new OpenApiExceptions.MissingRequiredParameterException("itemUuid", "query string")
        }
      api.addPrint(uuid, itemUuid)(request)
    }

    executeApi().map { result =>
      val json = Json.toJson(result)
      Ok(json)
    }
  }

  /**
    * PUT /api/printer/
    */
  def addPrinter(): Action[AnyContent] = Action.async { request =>
    def executeApi(): Future[Printer] = {
      api.addPrinter()(request)
    }

    executeApi().map { result =>
      val json = Json.toJson(result)
      Ok(json)
    }
  }

  /**
    * DELETE /api/print-job/:uuid
    */
  def deleteJob(uuid: String): Action[AnyContent] = Action.async { request =>
    def executeApi(): Future[PrintJob] = {
      api.deleteJob(uuid)(request)
    }

    executeApi().map { result =>
      val json = Json.toJson(result)
      Ok(json)
    }
  }

  /**
    * GET /api/printer/:uuid/
    */
  def deletePrinter(uuid: String): Action[AnyContent] = Action.async { request =>
    def executeApi(): Future[Unit] = {
      api.deletePrinter(uuid)(request)
    }

    executeApi().map { _ =>
      Ok
    }
  }

  /**
    * GET /api/printer/
    */
  def getPrinters(): Action[AnyContent] = Action.async { request =>
    def executeApi(): Future[List[Printer]] = {
      api.getPrinters()(request)
    }

    executeApi().map { result =>
      val json = Json.toJson(result)
      Ok(json)
    }
  }

  /**
    * GET /api/printer/:uuid/print-queue/
    */
  def getPrints(uuid: String): Action[AnyContent] = Action.async { request =>
    def executeApi(): Future[List[PrintJob]] = {
      api.getPrints(uuid)(request)
    }

    executeApi().map { result =>
      val json = Json.toJson(result)
      Ok(json)
    }
  }

  /**
    * PUT /api/print-job/:uuid/status?status=[value]
    */
  def setStatus(uuid: String): Action[AnyContent] = Action.async { request =>
    def executeApi(): Future[PrintJob] = {
      val status = request.getQueryString("status")
        .getOrElse {
          throw new OpenApiExceptions.MissingRequiredParameterException("status", "query string")
        }
      api.setStatus(uuid, status)(request)
    }

    executeApi().map { result =>
      val json = Json.toJson(result)
      Ok(json)
    }
  }

  /**
    * PUT /api/printer/:uuid/?newIdentifier=[value]
    */
  def updatePrinter(uuid: String): Action[AnyContent] = Action.async { request =>
    def executeApi(): Future[Printer] = {
      val newIdentifier = request.getQueryString("newIdentifier")
        .getOrElse {
          throw new OpenApiExceptions.MissingRequiredParameterException("newIdentifier", "query string")
        }
      api.updatePrinter(uuid, newIdentifier)(request)
    }

    executeApi().map { result =>
      val json = Json.toJson(result)
      Ok(json)
    }
  }

  private def splitCollectionParam(paramValues: String, collectionFormat: String): List[String] = {
    val splitBy =
      collectionFormat match {
        case "csv" => ",+"
        case "tsv" => "\t+"
        case "ssv" => " +"
        case "pipes" => "|+"
      }

    paramValues.split(splitBy).toList
  }
}
