import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Item, Link, LinkService} from "../../../../generated/api";
import {MatTableDataSource} from "@angular/material/table";
import {RxjsHelperService} from "../../../rxjs-helper.service";
import {HolderWithSubReference} from "../../holder-with-subreference-parent";
import {AppItemService} from "../../app-item.service";

@Component({
  selector: 'app-link-table',
  templateUrl: './link-table.component.html',
  styleUrls: ['./link-table.component.css']
})
export class LinkTableComponent extends HolderWithSubReference implements OnInit {
  private _item: Item;

  constructor(
    private linkService: LinkService,
    private r: RxjsHelperService,
    private appItemService: AppItemService,
  ) {
    super()
  }

  datasource = new MatTableDataSource<Link>()

  @Input() set item(i: Item) {
    this._item = i;
    this.refresh();
  }

  get item() {
    return this._item;
  }

  @Output() deleteLink = new EventEmitter<Link>();
  @Input() holder: boolean = false;
  @Input() holding: boolean = false;

  ngOnInit(): void {
    this.refresh()
  }


  getField(f: Link, uuid: string) {
    return f.fields.find(v => v.uuid === uuid)
  }

  updateDisplayedColumns(fieldCols: Array<string>) {
    this.displayedColumns = ['thumbnail', 'title', 'description'].concat(fieldCols, ['actions'])
  }

  refresh() {
    this.datasource.data = this.item.links.filter(v => v.holder === this.holder && v.holding === this.holding);
    this.item.links.flatMap(v => v.fields).forEach(v =>
      this.fields[v.uuid] = v.label
    )
    this.updateDisplayedColumns(this.fieldsIds)
  }

  delete(l: Link) {
    if (confirm("Are you sure you want to delete link")) {
      this.r.wrap(this.linkService.deleteLink(l.uuid))
        .withErrorMessage("Error while removing link")
        .then(v => this.deleteLink.emit(l))
    }
  }

  goTo(row) {
    this.appItemService.goTo(row.item);
  }
}
